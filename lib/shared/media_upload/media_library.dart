
import 'package:bridge/domain/model/media/media_type.dart';
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/main.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/services/utilities/util_service.dart';

class MediaLibrary extends StatefulWidget {

  final MediaType type;
  final Set<Media> search;

  const MediaLibrary({ Key? key, required this.type, required this.search }) : super(key: key);

  @override
  _MediaLibraryState createState() => _MediaLibraryState();
}

class _MediaLibraryState extends State<MediaLibrary> {

  final AudioPlayer _audioPlayer = AudioPlayer();

  late String _audioName;

  late Translate _intl;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _audioPlayer.stop();
    super.dispose();
  }

  _controlAudioPlayer({ required Media media }){
    if(media != null) {
      _audioPlayer.onPlayerStateChanged.listen((s) {
        if ( mounted && (s == PlayerState.completed || s == PlayerState.stopped)) {
          setState(() => media.isPlaying = false );
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return Observer(
      builder: (_) {

        return FutureBuilder(
          future: mainStore.mediaUpload.localMediaLibrary,
          builder: (context, snapshot) {

            switch(snapshot.connectionState) {
              case ConnectionState.none: {} break;
              case ConnectionState.active: {} break;
              case ConnectionState.waiting: {} break;
              case ConnectionState.done: {

                if(snapshot.hasData) {

                  Set<Media> audios = snapshot.data as Set<Media>;

                  return NotificationListener<OverscrollIndicatorNotification>(
                    onNotification: (overscroll) {
                      overscroll.disallowIndicator();

                      return false;
                    },
                    child: ListView.builder(
                      itemCount: widget.search.isEmpty ? audios.length : widget.search.length,
                      padding: const EdgeInsets.all(6),
                      itemBuilder: (context, position) {

                        Media audio = widget.search.isEmpty ? audios.elementAt(position) : widget.search.elementAt(position); //_audioService.createAudio(song: song, postedBy: widget.currentUser);
                        audio.isSaved = false;

                        _controlAudioPlayer(media: audio);

                        if(audio.title.isEmpty) {
                          _audioName = '${ _intl.translate('SCREEN.CAMERA.PUBLISH.AUDIO_SEARCH.FIELD.UNKNOWN_MEDIA') }';
                        } else {
                          _audioName = audio.title;
                        }

                        return GestureDetector(
                          onTap: () => _controlAudioStatus(audio: audio),
                          child: Container(
                            color: Colors.transparent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(bottom: 8),
                                      child: Container(
                                        width: 50,
                                        height: 50,
                                        decoration: BoxDecoration(
                                          image: audio.image == null || audio.image.isEmpty ?
                                            DecorationImage(image: AssetImage('assets/icons/headphone-white.png')) :
                                            DecorationImage(image: NetworkImage(audio.image)),
                                          color: Colors.grey, //Color.fromRGBO(230, 230, 230, 1),
                                          borderRadius: BorderRadius.circular(8),
                                        ),
                                        child: !audio.isPlaying ?
                                        Image.asset('assets/icons/arrow.png', color: Colors.black, scale: 3) :
                                        Image.asset('assets/icons/pause.png', scale: 1.5),
                                      ),
                                    ),
                                    const SizedBox(width: 12),
                                    Container(
                                      width: MediaQuery.of(context).size.width * 0.6,
                                      child: Text('${ _audioName }', maxLines: 1, overflow: TextOverflow.ellipsis, style: TextStyle(color: Colors.grey, fontSize: 15)),
                                    ),
                                  ],
                                ),
                                GestureDetector(
                                  onTap: () async => await _addLocalMedias(context: context, audio: audio),
                                  child: Visibility(
                                    visible: audio.isPlaying,
                                    child: Container(
                                      width: 50,
                                      height: 30,
                                      padding: EdgeInsets.all(2),
                                      decoration: BoxDecoration(
                                        color: Colors.black,
                                        border: Border.all(
                                          color: Colors.black,
                                          width: 0.5,
                                        ),
                                        borderRadius: BorderRadius.circular(100),
                                      ),
                                      child: const Icon(Icons.check,
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  );

                }
              } break;
            }

            return Loading();
          },
        );
      },
    );
  }

  _controlAudioStatus({ required Media audio }) {
    if(_audioPlayer.state == null || _audioPlayer.state.index == 0 || _audioPlayer.state.index == 3){
      if(!audio.isPlaying)
        _audioPlayer.play(new AssetSource(audio.path));
      else
        _audioPlayer.stop();
    } else
      _audioPlayer.stop();

    setState(() => audio.isPlaying = !audio.isPlaying );
  }

  _addLocalMedias({ required BuildContext context, required Media audio }) async {
    _controlAudioStatus(audio: audio);

    final num duration = audio.duration / 1000;

    if(duration >= 60 && duration <= 3600) { //3600 seconds = 1 hour
      if (widget.type.index == MediaType.AUDIOBOOK.index) {
        /*await mainStore.audiobook.addLocalChapters(
          chapter: Media.onlyMediaFile(mediaFile: audio.mediaFile),
        );*/
      } else {
        /*await mainStore.episode.addLocalEpisodes(
          episode: audio.mediaFile,
        );*/
      }

      Navigator.pop(context);
    } else {
      String audioType = 'Audio';
      if(widget.type.index == MediaType.AUDIOBOOK.index)
        audioType = 'Audiobook';
      else
        audioType = 'Podcast';

      UtilService.notify(context: context, message: '${ audioType } duration must be between 1 minute and 1 hour');
    }
  }
}
