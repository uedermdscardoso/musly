
import 'package:flutter/material.dart';
import 'package:bridge/Translate.dart';

class ModalDialog extends StatefulWidget {

  String title;
  String? positiveButtonName;
  String? negativeButtonName;

  ModalDialog({ required this.title, this.positiveButtonName, this.negativeButtonName });

  @override
  _ModalDialogState createState() => _ModalDialogState();
}

class _ModalDialogState extends State<ModalDialog> {

  late bool _pressed;
  late bool _positive;

  late Translate _intl;

  @override
  void initState() {
    _pressed = false;
    _positive = false;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return SimpleDialog(
      title: Text(widget.title,
        textAlign: TextAlign.center,
        style: TextStyle(height: 1.25, fontSize: 16, fontWeight: FontWeight.bold),
      ),
      elevation: 5,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
          child: ElevatedButton(
            onPressed: () => {
              Navigator.pop(context, false),
            },
            onFocusChange: (pressed) {
              setState(() => _positive = pressed);
            },
            style: ElevatedButton.styleFrom(backgroundColor: _positive ? Colors.white : Colors.black, textStyle: const TextStyle(fontSize: 20)),
            child: Text('${ widget.positiveButtonName ?? '${ _intl.translate('COMPONENT.MODAL_DIALOG.BUTTON.DEFAULT.YES') }' }',
              textAlign: TextAlign.center,
              style: TextStyle(height: 1.25, fontSize: 15, fontWeight: FontWeight.normal),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
          child: ElevatedButton(
          onPressed: () => {
              Navigator.pop(context, true),
            },
            onFocusChange: (pressed) {
              setState(() => _positive = pressed);
            },
            style: ElevatedButton.styleFrom(backgroundColor: _positive ? Colors.white : Colors.black, textStyle: const TextStyle(fontSize: 20)),
            child: Text('${ widget.negativeButtonName ?? '${ _intl.translate('COMPONENT.MODAL_DIALOG.BUTTON.DEFAULT.NO') }' }',
              textAlign: TextAlign.center,
              style: TextStyle(height: 1.25, fontSize: 15, fontWeight: FontWeight.normal),
            ),
          ),
        ),
      ],
    );
  }
}
