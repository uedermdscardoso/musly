
import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:bridge/domain/model/media/media_language.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/media/caption/custom_caption.dart';
import 'package:bridge/services/media/caption/caption_timestamp.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:path/path.dart';
import 'package:video_player/video_player.dart';

class WatsonService {

  static const String protocol = "http://";
  static const String host = "192.168.0.103"; //"reveals-server.herokuapp.com";
  static const num port = 8080;

  void recognize({ required MediaLanguage language, required File audio }) async {
    try {
      if(!(await audio.exists())) {
        throw new Exception("Original Audio not found");
      }

      final MediaService mediaService = MediaService();

      var postUri = Uri.parse("$protocol $host :$port /recognize/media/${ language.toString().split('.').last.toLowerCase() }");
      var request = http.MultipartRequest("POST", postUri);

      final String extension = audio.path.split(".").last;

      if(extension.compareTo("aac") == 0) {

        var stream = http.ByteStream.fromBytes(audio.readAsBytesSync());
        var multipartFile = http.MultipartFile('audio', stream, audio.lengthSync(), filename: basename(audio.path));

        request.files.add(multipartFile);

        request.send().then((response) {
          response.stream.transform(utf8.decoder).listen((value) {
            if(value != null) {
              List<CustomCaption> captions = _createObject(value);

              mainStore.watson.addCaptions(captions: captions);
            }
          }).onError((e) {
            throw Exception(e);
          });
        });
      } else {
        throw Exception("Modified Audio not found");
      }
    } catch(e) {
      throw Exception("Wav not found");
    }
  }

  List<CustomCaption> _createObject(String value) {
    final Map<String, dynamic> obj = json.decode(value);
    List<CustomCaption> captions = [];
    List<dynamic> content = obj['content'];
    int wordDefault = 15;

    content.forEach((item) {
      String transcript = item['transcript'];
      String timestamps = item['timestamps'];
      List<String> timestampList = timestamps.replaceAll("|", "\"").split("(*)");
      List<CaptionTimestamp> times = _createTimestamp(timestampList);

      if(transcript.isNotEmpty) {

        if(times.length > wordDefault) {

          List<String> words = transcript.split(" ");
          List<List<String>> modifiedWords = [];
          List<List<CaptionTimestamp>> modifiedTimes = [];

          String part = (times.length / wordDefault).toString();
          int inteiro = int.parse(part.indexOf(".") != -1 ? part.substring(0,part.indexOf(".")) : part);
          int resto = wordDefault - inteiro;

          for(int i=0; i<inteiro; i++) {
            modifiedWords.add(words.skip(i * wordDefault).take(wordDefault).toList());
            modifiedTimes.add(times.skip(i * wordDefault).take(wordDefault).toList());
          }
          modifiedWords.add(words.skip(inteiro * wordDefault).take(resto).toList());
          modifiedTimes.add(times.skip(inteiro * wordDefault).take(resto).toList());

          modifiedTimes.asMap().forEach((index, obj) {
            captions.add(CustomCaption(transcript: modifiedWords.elementAt(index).join(" "), timestamps: obj));
          });
        } else {
          captions.add(CustomCaption(transcript: transcript, timestamps: times));
        }
      }

    });

    return captions;
  }

  List<CaptionTimestamp> _createTimestamp(List<String> timestampList) {
    String newTimestamp = "";

    timestampList.forEach((timestamp) {
      newTimestamp += "{${ timestamp }},";
    });

    newTimestamp = newTimestamp.substring(0, newTimestamp.lastIndexOf(","));
    newTimestamp = "[${ newTimestamp }]";

    List<CaptionTimestamp> timestamps = (json.decode(newTimestamp) as List)
        .map((timestamp) => CaptionTimestamp.fromJson(timestamp))
        .toList();

    return timestamps;
  }

  /*List<CaptionTimestamp> extractTimestamps({ List<CustomCaption> captions }) {
    List<CaptionTimestamp> timestamps = [];

    captions.forEach((caption) {
      timestamps.addAll(caption.timestamps);
    });
  }*/
}