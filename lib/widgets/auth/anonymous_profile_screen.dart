
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/AppLanguage.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/auth/phone_number_check.dart';
import 'package:country_codes/country_codes.dart';
import 'package:bridge/domain/model/utilities/login_type.dart';
import 'package:bridge/widgets/header.dart';
import 'package:provider/provider.dart';

class AnonymousProfileScreen extends StatefulWidget {
  @override
  _AnonymousProfileScreenState createState() => _AnonymousProfileScreenState();
}

class _AnonymousProfileScreenState extends State<AnonymousProfileScreen> {

  final List<String> _items = ['en','pt','es'];

  late String _selectedItem;
  late bool _isPressed;

  late AppLanguage _appLanguage;

  late Translate _intl;

  @override
  void initState() {
    _selectedItem = '';
    _isPressed = false;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _appLanguage = Provider.of<AppLanguage>(context);
    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.grey[100],
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.grey[100],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            /*Header(
              color: Colors.transparent,
              hasBorder: false,
            ),*/
            Padding(
              padding: const EdgeInsets.only(top: 45, left: 30, right: 30),
              child: Align(
                alignment: Alignment.centerRight,
                child: DropdownButton<String>(
                  elevation: 5,
                  icon: Icon(Icons.arrow_drop_down, color: Colors.grey),
                  style: TextStyle(color: Colors.grey),
                  value: _intl.locale.languageCode.toLowerCase(),
                  underline: Container(),
                  onChanged: (String? value) => {
                    setState(() => _selectedItem = value! ),
                  },
                  items: _items.map<DropdownMenuItem<String>>((String item) {

                    String translated = _translate(item: item);

                    return DropdownMenuItem<String>(
                      value: item,
                      child: GestureDetector(
                        onTap: () async => await _makeAction(context: context, item: item),
                        child: Container(
                          child: Text(translated, style: TextStyle(color: Colors.grey)),
                        ),
                      ),
                    );
                  }).toList(),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 52, right: 52),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('${ _intl.translate('SCREEN.LOGIN.WELCOME.TITLE') }',
                        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25)),
                    Padding(
                      padding: const EdgeInsets.only(top: 50, bottom: 50),
                      child: Column(
                        children: [
                          Text('${ _intl.translate('SCREEN.LOGIN.WELCOME.MESSAGE.FIRST_PART') }',
                            textAlign: TextAlign.justify,
                            style: const TextStyle(
                              height: 2.25,
                              color: Colors.black54,
                              fontWeight: FontWeight.normal,
                              fontSize: 16,
                            ),
                          ),
                          const SizedBox(height: 8),
                          Text('${ _intl.translate('SCREEN.LOGIN.WELCOME.MESSAGE.LAST_PART') }',
                            textAlign: TextAlign.justify,
                            style: const TextStyle(
                              height: 2.25,
                              color: Colors.black54,
                              fontWeight: FontWeight.normal,
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 20),
                    Container(
                      width: 250,
                      child: ElevatedButton(
                        onPressed: () async => await _checkPhoneNumber(context: context, type: LoginType.BY_WAITING_LIST),
                        onFocusChange: (pressed){
                          setState(() => _isPressed = pressed);
                        },
                        child: Text('${ _intl.translate('SCREEN.LOGIN.WELCOME.ACTION.ACCESS_YOUR_ACCOUNT') }', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                        style: ElevatedButton.styleFrom(
                          elevation: 0,
                          backgroundColor: Colors.white,
                          padding: EdgeInsets.all(15),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                          textStyle: TextStyle(color: Colors.black),
                        ),
                      ),
                    ),
                    const SizedBox(height: 18),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('${ _intl.translate('SCREEN.LOGIN.WELCOME.MESSAGE.HAVE_AN_INVITE') }', style: TextStyle(color: Colors.black54, fontSize: 14)),
                        const SizedBox(width: 6),
                        GestureDetector(
                          onTap: () async => await _checkPhoneNumber(context: context, type: LoginType.BY_INVITE),
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 0.25),
                            child: Container(
                              child: Text('${ _intl.translate('SCREEN.LOGIN.WELCOME.ACTION.SIGN_IN') }',
                                style: TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 14)),
                            ),
                          ),
                        ),
                      ],
                    ),
                    //const SizedBox(height: 60),
                      /*Padding(
                          padding: const EdgeInsets.only(bottom: 40),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text('${ _intl.translate('SCREEN.LOGIN.SOCIAL_NETWORT.REGISTER') }:',
                                style: TextStyle(color: Colors.black54, fontSize: 15.5, fontFamily: 'Arial'),
                              ),
                              const SizedBox(height: 16),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset('assets/icons/social_network/dark/twitter_dark.png', scale: 1.5),
                                  const SizedBox(width: 8),
                                  Image.asset('assets/icons/social_network/dark/facebook_dark.png', scale: 1.5),
                                  const SizedBox(width: 8),
                                  Image.asset('assets/icons/social_network/dark/google_dark.png', scale: 1.5),
                                ],
                              ),
                            ],
                      ),
                    ),*/
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _translate({ required String item }) {
    switch(item) {
      case 'en':
        return 'English';
        break;
      case 'pt':
        return 'Português';
        break;
      case 'es':
        return 'Español';
        break;
    };
  }

  _makeAction({ required BuildContext context, required String item }) async {
    switch(item) {
      case 'en':
        _appLanguage.changeLanguage(Locale('en'));
        break;
      case 'pt':
        _appLanguage.changeLanguage(Locale('pt'));
        break;
      case 'es':
        _appLanguage.changeLanguage(Locale('es'));
        break;
    };
  }

  _checkPhoneNumber({ required BuildContext context, required LoginType type }) async {
    await CountryCodes.init();

    final CountryDetails details = CountryCodes.detailsForLocale();

    Navigator.of(context).push(UtilService.createRoute(
      PhoneNumberCheck(details: details, type: type),
    ));
  }
}
