
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter/material.dart';
import "package:collection/collection.dart";
import 'package:flutter/services.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/chat/chat.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/services/chat/chat_service.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/profile_photo.dart';
import 'package:bridge/widgets/auth/anonymous_profile_screen.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/widgets/chat/chat_screen.dart';
import 'package:bridge/widgets/chat/follower_group.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/shared/no_result_found.dart';
import 'package:bridge/shared/internet_loading_failed.dart';

class ConversationGroup extends StatefulWidget {

  const ConversationGroup({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ConversationGroupState();
}

class _ConversationGroupState extends State<ConversationGroup> {

  final _authService = AuthenticationService();
  final _chatService = ChatService();
  final _userService = UserService();

  final List<String> _items = ['remove'];

  late Users _currentUser;
  late Translate _intl;

  @override
  void initState() {
    _init();
    _addConversations();
   super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init() {
    _currentUser = Users.onlyId(id: mainStore.auth.currentUser.id);
  }

  _addConversations() {
    mainStore.chat.addConversations();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarDividerColor: Colors.black,
      ),
      child: Material(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Header(
              color: Colors.transparent,
              left: Text('${ _intl.translate('SCREEN.NOTIFICATION.CHAT.TITLE') }', style: TextStyle(color: Colors.black, decoration: TextDecoration.none, fontSize: 18, fontWeight: FontWeight.bold, fontFamily: 'Arial')),
              hasBorder: true,
            ),
            const SizedBox(height: 15),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Observer(
                      builder: (_) {

                        return Padding(
                          padding: const EdgeInsets.all(12),
                          child: StreamBuilder(
                            stream: mainStore.chat.conversations,
                            builder: (context, snapshot) {

                              switch(snapshot.connectionState) {
                                case ConnectionState.none:
                                  return NoResultFound(icon: Icons.chat_bubble_outline);
                                  //return InternetLoadingFailed();
                                  break;
                                case ConnectionState.waiting:
                                  return Loading();
                                  break;
                                case ConnectionState.done:
                                  break;
                                case ConnectionState.active: {

                                  if(snapshot.hasData){

                                    final QuerySnapshot query = snapshot.data as QuerySnapshot;

                                    return FutureBuilder(
                                      future: _chatService.makeList(data: query),
                                      builder: (context, snapshot) {

                                        if(snapshot.hasData){

                                          final List<Chat> _conversations = snapshot.data as List<Chat>;
                                          final conversationsGroupBy = groupBy(_conversations, (Chat chat) => chat.chatId);

                                          if(conversationsGroupBy.isNotEmpty) {

                                            return NotificationListener<OverscrollIndicatorNotification>(
                                              onNotification: (overscroll) {
                                                overscroll.disallowIndicator();

                                                return false;
                                              },
                                              child: ListView.builder(
                                                itemCount: conversationsGroupBy.length,
                                                physics: const PageScrollPhysics(),
                                                scrollDirection: Axis.vertical,
                                                padding: const EdgeInsets.all(0),
                                                itemBuilder: (context, position) {
                                                  final List<Chat> c = conversationsGroupBy.values.elementAt(position);

                                                  final String friendId = c.elementAt(0).to.compareTo(_currentUser.id) != 0? c.elementAt(0).to : c.elementAt(0).from;

                                                  final Future<Users> users = _userService.getUserById(user: Users.onlyId(id: friendId));

                                                  return FutureBuilder(
                                                    future: users,
                                                    builder: (context, snapshot){

                                                      if(snapshot.hasData){

                                                        final Users friend = snapshot.data as Users;

                                                        return GestureDetector(
                                                          onTap: () => Navigator.of(context).push(UtilService.createRoute(
                                                              ChatScreen(currentUser: _currentUser, to: Users.onlyId(id: friendId ) ))
                                                          ),
                                                          child: Padding(
                                                            padding: const EdgeInsets.only(top: 12),
                                                            child: Container(
                                                              decoration: const BoxDecoration(
                                                                border: Border(bottom: BorderSide(color: Colors.grey, width: 0.2)),
                                                              ),
                                                              child: Row(
                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                children: [
                                                                  Row(
                                                                    children: [
                                                                      Padding(
                                                                        padding: const EdgeInsets.only(bottom: 13.5),
                                                                        child: ProfilePhoto(user: friend, width: 55, height: 55),
                                                                      ),
                                                                      //const SizedBox(width: 5),
                                                                      Padding(
                                                                        padding: const EdgeInsets.only(top: 8, bottom: 25, left: 15, right: 22),
                                                                        child: Column(
                                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                                          children: [
                                                                            Row(
                                                                              children: [
                                                                                Text('${ friend.firstName } ',
                                                                                  style: TextStyle(fontSize: 16, color: Colors.grey[700], decoration: TextDecoration.none, fontWeight: FontWeight.bold),
                                                                                ),
                                                                                Text('${ friend.lastName } ',
                                                                                  style: TextStyle(fontSize: 16, color: Colors.grey[700], decoration: TextDecoration.none, fontWeight: FontWeight.bold),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                            const SizedBox(height: 5),
                                                                            Text('@${ friend.username }',
                                                                              style: TextStyle(fontSize: 16, color: Colors.grey, decoration: TextDecoration.none, fontWeight: FontWeight.normal, fontFamily: 'Arial'),
                                                                            ),
                                                                            const SizedBox(height: 5),
                                                                            Container(
                                                                              width: MediaQuery.of(context).size.width * 0.5,
                                                                              child: Text(
                                                                                c.elementAt(0).message,
                                                                                overflow: TextOverflow.ellipsis,
                                                                                style: TextStyle(fontSize: 14, color: Colors.grey, decoration: TextDecoration.none, fontWeight: FontWeight.normal, fontFamily: 'Arial'),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  DropdownButton<String>(
                                                                    icon: const Icon(Icons.more_vert),
                                                                    iconSize: 25,
                                                                    elevation: 0,
                                                                    style: const TextStyle(color: Colors.grey),
                                                                    onChanged: (_) {},
                                                                    underline: Container(),
                                                                    items: _items.map<DropdownMenuItem<String>>((String item) {

                                                                      String translated = _translate(item: item);

                                                                      return DropdownMenuItem<String>(
                                                                        value: item,
                                                                        child: GestureDetector(
                                                                          onTap: () async => await _makeAction(item: item, friend: friend),
                                                                          child: Container(
                                                                            child: Text(translated),
                                                                          ),
                                                                        ),
                                                                      );
                                                                    }).toList(),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        );
                                                      }

                                                      return Container();
                                                      //return NoResultFound(icon: Icons.chat_bubble_outline);

                                                    },
                                                  );
                                                },
                                              ),
                                            );
                                          }
                                        }

                                        return Container();
                                        //return NoResultFound(icon: Icons.chat_bubble_outline);

                                      },
                                    );
                                  }
                                } break;
                              }

                              return Container();
                              //return NoResultFound(icon: Icons.chat_bubble_outline);
                            },
                          ),
                        );
                      }
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 25, bottom: 75),
                      child: GestureDetector(
                        onTap: () async {
                          if(await _authService.isAnonymous())
                            Navigator.of(context).push(UtilService.createRoute(AnonymousProfileScreen()));
                          else
                            Navigator.of(context).push(UtilService.createRoute(FollowerGroup(currentUser: _currentUser)));
                        },
                        child: Container(
                          padding: EdgeInsets.all(18),
                          decoration: BoxDecoration(
                            color: Colors.black,
                            borderRadius: BorderRadius.circular(100),
                          ),
                          child: Image.asset('assets/icons/plus.png', scale: 3.5, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _translate({ required String item }) {
    switch(item) {
      case 'remove':
        return '${ _intl.translate('SCREEN.NOTIFICATION.CHAT.BUTTON.REMOVE') }';
        break;
    };
  }

  _makeAction({ required String item, required Users friend }) async {
    switch(item) {
      case 'remove':
        await _chatService.removeChat(from: _currentUser.id, to: friend.id);
        break;
    };
  }
}
