
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/widgets/header.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:firebase_auth/firebase_auth.dart';

class DescriptionEdit extends StatefulWidget {
  @override
  _DescriptionEditState createState() => _DescriptionEditState();
}

class _DescriptionEditState extends State<DescriptionEdit> {

  final _authService = AuthenticationService();

  final _descriptionController = TextEditingController();
  final _userService = UserService();

  Users _currentUser = Users();

  late Translate _intl;

  @override
  void initState() {
    mainStore.user.addCurrentUser();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Column(
          children: [
            Header(
              color: Colors.transparent,
              left: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                    ),
                  ),
                  const SizedBox(width: 4),
                  Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_DESCRIPTION.TITLE') }',
                    style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
                ],
              ),
              right: GestureDetector(
                onTap: () async => await _editDescription(context: context),
                child: Container(
                    child: Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_DESCRIPTION.BUTTON.SAVE') }', style: TextStyle(color: Colors.black, fontSize: 15))
                ),
              ),
              hasBorder: true,
            ),
            Observer(
              builder: (_) {

                return StreamBuilder(
                  stream: mainStore.user.currentUser,
                  builder: (context, snapshot) {

                    if(snapshot.hasData) {
                      final QuerySnapshot snap = snapshot.data as QuerySnapshot;
                      _currentUser = _userService.makeUser(doc: snap.docs.first);
                    }

                    _descriptionController.text = _currentUser.description;

                    return Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(16),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 12, left: 8, right: 8, bottom: 8),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_DESCRIPTION.TIP') }', style: TextStyle(fontSize: 12, color: Colors.grey)),
                              ),
                            ),
                            CustomField(
                              controller: _descriptionController,
                              autofocus: false,
                              textColor: Colors.grey,
                              borderColor: Colors.grey[300],
                              backgroundColor: Colors.white,
                              maxLines: 8,
                              text: '${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_DESCRIPTION.FIELD.DESCRIPTION') }',
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              }
            ),
          ],
        ),
      ),
    );
  }

  _editDescription({ required BuildContext context }) async {
    if(_descriptionController != null && _descriptionController.text.isNotEmpty) {
      if(_descriptionController.text.length <= 20){
        final String description = _descriptionController.text;

        if(!_authService.isAnonymous()) {
          final User currentUser = await FirebaseAuth.instance.currentUser!;

          if(currentUser != null && currentUser.uid != null) {
            await _userService.updateDescriptionByUser(
              user: Users.onlyIdAndDescription(
                id: currentUser.uid,
                description: description,
              ),
            );
          }
        }

        Navigator.pop(context);

        UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_DESCRIPTION.NOTIFICATION.SUCCESS') }');
      } else
        UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_DESCRIPTION.NOTIFICATION.SIZE_INVALID') }');

    } else
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_DESCRIPTION.NOTIFICATION.INVALID') }');
  }
}
