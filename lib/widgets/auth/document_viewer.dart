
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:bridge/domain/model/utilities/report_type.dart';
import 'package:bridge/widgets/header.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:connectivity/connectivity.dart';
import 'package:bridge/shared/internet_loading_failed.dart';
import 'package:flutter/services.dart';
import 'package:bridge/Translate.dart';

class DocumentViewer extends StatefulWidget {

  final ReportType type;

  const DocumentViewer({ Key? key, required this.type }) : super(key: key);

  @override
  _DocumentViewerState createState() => _DocumentViewerState();
}

class _DocumentViewerState extends State<DocumentViewer> {

  static const String _privacy_policy_url = "https://musly.notion.site/Privacy-Policy-badc3109bcf1449a84ade7becbbedbf5";
  static const String _terms_of_use = "https://musly.notion.site/Terms-of-Use-156a7e4e39684622b03de602ed55bb16";

  final Completer<WebViewController> _controller = Completer<WebViewController>();

  bool _isChecked = false;

  late WebViewController _webViewController;
  late Translate _intl;

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return WillPopScope(
      onWillPop: () async {
        if(_isChecked)
          return true;

        UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.DOCUMENT_VIEWER.NOTIFICATION.NO_READ') } ${ widget.type.index == ReportType.TERMS_OF_USE.index ?
          _intl.translate('SCREEN.DOCUMENT_VIEWER.TERM.TERMS_OF_USE') : _intl.translate('SCREEN.DOCUMENT_VIEWER.TERM.PRIVACY_POLICY') }',
        );

        return false;
      },
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.dark,
          statusBarIconBrightness: Brightness.dark,
          systemNavigationBarColor: Colors.grey[100],
          systemNavigationBarIconBrightness: Brightness.dark,
          systemNavigationBarDividerColor: Colors.white,
        ),
        child: Material(
          color: Colors.white,
          child: Column(
            children: [
              Header(
                left: GestureDetector(
                  onTap: () {
                    if(_isChecked)
                      Navigator.pop(context);
                    else {
                      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.DOCUMENT_VIEWER.NOTIFICATION.NO_READ') } ${ widget.type.index == ReportType.TERMS_OF_USE.index ?
                       _intl.translate('SCREEN.DOCUMENT_VIEWER.TERM.TERMS_OF_USE') : _intl.translate('SCREEN.DOCUMENT_VIEWER.TERM.PRIVACY_POLICY') }',
                      );
                    }
                  },
                  child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                ),
                color: Colors.transparent,
                hasBorder: true,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 12, bottom: 12, left: 12, right: 12),
                  child: Column(
                    children: [
                      Expanded(
                        child: FutureBuilder(
                          future: Connectivity().checkConnectivity(),
                          builder: (context, snapshot) {

                            if(snapshot.hasData) {

                              final ConnectivityResult connectivityResult = snapshot.data as ConnectivityResult;

                              if(connectivityResult != ConnectivityResult.none) {

                                return Container(
                                  decoration: BoxDecoration(
                                    color: Colors.grey[100],
                                    borderRadius: BorderRadius.circular(18),
                                  ),
                                  child: Visibility(
                                    visible: widget.type.index == ReportType.PRIVACY_POLICY.index,
                                    child: WebViewWidget(
                                      controller: WebViewController()
                                        ..setJavaScriptMode(JavaScriptMode.unrestricted)
                                        ..setBackgroundColor(Colors.white)
                                        ..setNavigationDelegate(
                                            NavigationDelegate(
                                              onProgress: (int progress) {
                                                // Update loading bar.
                                              },
                                              onPageStarted: (String url) {
                                                print('Page started loading: $url');
                                              },
                                              onPageFinished: (String url) {
                                                print('Page finished loading: $url');
                                              },
                                              onWebResourceError: (WebResourceError error) {},
                                              onNavigationRequest: (NavigationRequest request) {
                                                if (request.url.startsWith(_privacy_policy_url)) {
                                                  return NavigationDecision.prevent;
                                                }
                                                return NavigationDecision.navigate;
                                              },
                                            ),
                                      ),
                                    ),
                                    replacement: WebViewWidget(
                                      controller: WebViewController()
                                        ..setJavaScriptMode(JavaScriptMode.unrestricted)
                                        ..setBackgroundColor(Colors.white)
                                        ..setNavigationDelegate(
                                          NavigationDelegate(
                                            onProgress: (int progress) {
                                              // Update loading bar.
                                            },
                                            onPageStarted: (String url) {
                                              print('Page started loading: $url');
                                            },
                                            onPageFinished: (String url) {
                                              print('Page finished loading: $url');
                                            },
                                            onWebResourceError: (WebResourceError error) {},
                                            onNavigationRequest: (NavigationRequest request) {
                                              if (request.url.startsWith(_terms_of_use)) {
                                                return NavigationDecision.prevent;
                                              }
                                              return NavigationDecision.navigate;
                                            },
                                          ),
                                        ),
                                    ),
                                  ),
                                );
                              } else
                                return InternetLoadingFailed();
                            }

                            return Container();
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 30, bottom: 50),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Checkbox(
                                  checkColor: Colors.white,
                                  focusColor: Colors.black,
                                  activeColor: Colors.black,
                                  value: _isChecked,
                                  onChanged: (bool? value) {
                                    setState(() => _isChecked = value!);
                                  },
                                ),
                                Text('${ _intl.translate('SCREEN.DOCUMENT_VIEWER.FIELD.ACCEPTED') } ${ widget.type.index == ReportType.TERMS_OF_USE.index ?
                                   _intl.translate('SCREEN.DOCUMENT_VIEWER.TERM.TERMS_OF_USE') : _intl.translate('SCREEN.DOCUMENT_VIEWER.TERM.PRIVACY_POLICY') }',
                                  style: const TextStyle(color: Colors.black, fontWeight: FontWeight.normal),
                                ),
                              ],
                            ),
                            const SizedBox(height: 12),
                            GestureDetector(
                              onTap: () {
                                if(_isChecked)
                                  Navigator.pop(context);
                                else {
                                  UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.DOCUMENT_VIEWER.NOTIFICATION.NO_READ') } ${ widget.type.index == ReportType.TERMS_OF_USE.index ?
                                    _intl.translate('SCREEN.DOCUMENT_VIEWER.TERM.TERMS_OF_USE') : _intl.translate('SCREEN.DOCUMENT_VIEWER.TERM.PRIVACY_POLICY') }',
                                  );
                                }
                              },
                              child: Container(
                                width: 175,
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.circular(25),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(15),
                                  child: Text(_intl.translate('SCREEN.DOCUMENT_VIEWER.BUTTON.OK'), textAlign: TextAlign.center,
                                    style: const TextStyle(fontSize: 18, color: Colors.white,
                                      fontWeight: FontWeight.bold, decoration: TextDecoration.none)),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
