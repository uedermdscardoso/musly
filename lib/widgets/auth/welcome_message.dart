
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/main.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/auth/phone_number_check.dart';
import 'package:country_codes/country_codes.dart';
import 'package:bridge/domain/model/utilities/login_type.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/Translate.dart';

class WelcomeMessage extends StatefulWidget {
  @override
  _WelcomeMessageState createState() => _WelcomeMessageState();
}

class _WelcomeMessageState extends State<WelcomeMessage> {

  final UserService _userService = UserService();

  Users _user = Users();

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init() {
    mainStore.auth.addLocalUser();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.dark,
          statusBarIconBrightness: Brightness.dark,
          systemNavigationBarColor: Colors.grey[100],
          systemNavigationBarIconBrightness: Brightness.dark,
          systemNavigationBarDividerColor: Colors.white,
        ),
        child: Material(
          color: Colors.grey[100],
          child: Padding(
            padding: const EdgeInsets.only(top: 45,left: 52, right: 52),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset('assets/icons/congratulations.png', scale: 1.5),
                const SizedBox(height: 30),
                FutureBuilder(
                  future: mainStore.auth.localUser,
                  builder: (context, snapshot) {

                    if(snapshot.hasData)
                      _user = snapshot.data as Users;

                    return RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        style: const TextStyle(
                          height: 1.75,
                          color: Colors.grey,
                          fontSize: 18,
                        ),
                        children: <TextSpan>[
                          TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.WELCOME_MESSAGE.MESSAGE.FIRST') } '),
                          TextSpan(text: _user != null && _user.username != null && _user.username.isNotEmpty ?
                             '@${ _user.username } ' : '',
                            style: const TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold)),
                          TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.WELCOME_MESSAGE.MESSAGE.LAST') }'),
                        ],
                      ),
                    );
                  },
                ),
                const SizedBox(height: 30),
                GestureDetector(
                  onTap: () => _checkPhoneNumber(context: context, newUser: _user),
                  child: Container(
                    width: 175,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(25),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(15),
                      child: Text('${ _intl.translate('SCREEN.LOGIN.WELCOME_MESSAGE.ACTION.TRY_IT') }', textAlign: TextAlign.center,
                          style: const TextStyle(fontSize: 18, color: Colors.white,
                              fontWeight: FontWeight.bold, decoration: TextDecoration.none)),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _checkPhoneNumber({ required Users newUser, required BuildContext context }) async {
    await CountryCodes.init();

    final CountryDetails details = CountryCodes.detailsForLocale();

    if(newUser.phone != null)
      await _userService.allowAccess(user: newUser);

    Navigator.of(context).push(UtilService.createRoute(
      PhoneNumberCheck(details: details, type: LoginType.BY_WAITING_LIST),
    ));
  }
}
