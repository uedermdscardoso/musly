
import 'dart:collection';

import 'package:bridge/domain/model/media/media_type.dart';
import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:bridge/widgets/media/media_preview.dart';
import 'package:bridge/widgets/profile/profile_details.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/domain/model/utilities/mode_viewer_type.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/profile_photo.dart';
import 'package:bridge/widgets/media/media_group.dart';
import 'package:bridge/main.dart';
import 'package:bridge/Translate.dart';

class MediaDetails extends StatefulWidget {

  late bool isDemo;
  late bool isOriginal;
  late Media media;
  late MediaType type;

  MediaDetails({
    Key? key,
    this.isDemo : false,
    bool isOriginal : true,
    this.type : MediaType.SHORT_FORM_AUDIO,
    required this.media,
  }) : super(key: key);

  @override
  _MediaDetailsState createState() => _MediaDetailsState();
}

class _MediaDetailsState extends State<MediaDetails> {

  final _userService = UserService();
  final _authService = AuthenticationService();

  Users _author = Users();
  bool _isFollowing = false;

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init () {
    _author = widget.media.createdBy!;
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return Padding(
      padding: const EdgeInsets.only(bottom: 50),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () => _seeProfile(context: context, author: _author),
                          child: ProfilePhoto(
                            width: 70,
                            height: 70,
                            user: _author,
                          ),
                        ),
                        const SizedBox(width: 12),
                        Expanded(
                          child: GestureDetector(
                            onTap: () async => await _seeProfile(context: context, author: _author),
                            child: Column(
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text('${ _author.firstName } ${ _author.lastName }',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold, fontFamily: "Arial", decoration: TextDecoration.none)
                                  ),
                                ),
                                const SizedBox(height: 6),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text('@${ _author.username }',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold, fontFamily: "Arial", decoration: TextDecoration.none)
                                  ),
                                ),
                                const SizedBox(height: 12),
                                Align(
                                  alignment: Alignment.bottomLeft,
                                  child: StreamBuilder(
                                    stream: _userService.isFollowing(followed: _author),
                                    builder: (context, snapshot) {

                                      if(snapshot.hasData) {
                                        final QuerySnapshot snap = snapshot.data as QuerySnapshot;

                                        _isFollowing = snap.docs.isNotEmpty;
                                      }

                                      return Visibility(
                                        visible: _authService.isAnonymous() || FirebaseAuth.instance.currentUser!.uid != _author.id,
                                        child: Visibility(
                                          visible: _isFollowing,
                                          child: GestureDetector(
                                            onTap: () async => await _disfollow(context: context, author: _author),
                                            child: Container(
                                              width: 100,
                                              decoration: BoxDecoration(
                                                border: Border.all(width: 0.5, color: Colors.white),
                                                borderRadius: BorderRadius.circular(6),
                                                color: Colors.white,
                                              ),
                                              child: Padding(
                                                padding: const EdgeInsets.all(5),
                                                child: Align(
                                                  alignment: Alignment.center,
                                                  child: Text('${ _intl.translate('SCREEN.MEDIA.ALBUM.DETAILS.ACTION.FOLLOWING') }',
                                                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal, fontSize: 12, fontFamily: 'Arial', decoration: TextDecoration.none)),
                                                ),
                                              ),
                                            ),
                                          ),
                                          replacement: GestureDetector(
                                            onTap: () async => await _follow(context: context, author: _author),
                                            child: Container(
                                              width: 100,
                                              decoration: BoxDecoration(
                                                color: Colors.transparent,
                                                border: Border.all(width: 0.5, color: Colors.grey),
                                                borderRadius: BorderRadius.circular(6),
                                              ),
                                              child: Padding(
                                                padding: const EdgeInsets.all(5),
                                                child: Align(
                                                  alignment: Alignment.center,
                                                  child: Text('${ _intl.translate('SCREEN.MEDIA.ALBUM.DETAILS.ACTION.FOLLOW') }',
                                                      style: TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 12, fontFamily: 'Arial', decoration: TextDecoration.none)),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        replacement: GestureDetector(
                                          onTap: () => _follow(context: context, author: _author),
                                          child: Container(
                                            width: 100,
                                            decoration: BoxDecoration(
                                              border: Border.all(width: 0.5, color: Colors.white),
                                              borderRadius: BorderRadius.circular(6),
                                              color: Colors.transparent,
                                            ),
                                            child: Padding(
                                              padding: const EdgeInsets.only(top: 5, bottom: 5, left: 12, right: 12),
                                              child: Align(
                                                alignment: Alignment.center,
                                                child: Text('${ _intl.translate('SCREEN.MEDIA.ALBUM.DETAILS.ACTION.FOLLOW') }',
                                                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 12, fontFamily: 'Arial', decoration: TextDecoration.none)),
                                              ),
                                            ),
                                          ),
                                        ),
                                      );

                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Visibility(
                              visible: widget.type == MediaType.SHORT_FORM_AUDIO,
                              child: Container(),
                            ),
                            Visibility(
                              visible: widget.type == MediaType.STITCHED_AUDIO,
                              child: Row(
                                children: [
                                  Text('Stitch', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                  const SizedBox(width: 8),
                                  GestureDetector(
                                    onTap: () => _openOriginalAudio(context: context),
                                    child: Padding(
                                      padding: const EdgeInsets.only(right: 12),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(4),
                                          border: const Border(
                                            bottom: BorderSide(color: Colors.white, width: 0.35),
                                          ),
                                        ),
                                        child: Visibility(
                                          child: Text('${ widget.media != null ? widget.media.originalMedia!.author : 'creator' }',
                                              maxLines: 1,
                                              textAlign: TextAlign.start,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Visibility(
                              visible: widget.type == MediaType.LINKED_AUDIO,
                              child: Padding(
                                padding: const EdgeInsets.only(top: 2, bottom: 2),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 2),
                                      child: Container(
                                        color: Colors.transparent,
                                        width: 40,
                                        child: Visibility(
                                          child: Text('${ widget.media != null ? widget.media.originalMedia!.author : 'creator' }',
                                              maxLines: 1,
                                              textAlign: TextAlign.start,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () => _openContent(context: context),
                                      child: Padding(
                                        padding: const EdgeInsets.only(right: 12),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(4),
                                            border: const Border(
                                              bottom: BorderSide(color: Colors.white, width: 0.35),
                                            ),
                                          ),
                                          child: Text('${ widget.media != null ? widget.media.linkType : 'media' }',
                                            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Visibility(
                              visible: widget.type == MediaType.DUET,
                              child: Container(),
                            ),
                            Visibility(
                              visible: widget.type == MediaType.LIVE,
                              child: Container(),
                            ),
                            const SizedBox(width: 8),
                          ],
                        ),
                        const SizedBox(width: 18),
                        FutureBuilder(
                          future: Future.value(widget.media),
                          builder: (context, snapshot) {

                            if(snapshot.hasData) {

                              final Media media = snapshot.data as Media;

                              if(media.message != null && media.message.isNotEmpty) {
                                final List<String> words = media.message.split(" ");

                                return Align(
                                  alignment: Alignment.topLeft,
                                  child: RichText(
                                    maxLines: 3,
                                    textAlign: TextAlign.start,
                                    overflow: TextOverflow.ellipsis,
                                    text: TextSpan(
                                      style: TextStyle(height: 1.5),
                                      children: List.generate(words.length, (index) {

                                        final String word = words.elementAt(index);

                                        return TextSpan(
                                          text: "${ word } ",
                                          recognizer: new TapGestureRecognizer()..onTap = () {

                                          },
                                          style: TextStyle(
                                            fontWeight: word.contains("#") ? FontWeight.bold : FontWeight.normal,
                                            fontSize: 15,
                                            height: 1.25,
                                          ),
                                        );
                                      }),
                                    ),
                                  ),
                                );
                              }
                            }

                            return Container();
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _follow({ required BuildContext context, required Users author }) async {
    if(widget.isDemo) {
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.MEDIA.ALBUM.DETAILS.NOTIFICATION.DEMO') }');
      return;
    }

    if(!_authService.isAnonymous()) {
      await mainStore.user.follow(author: author);

      setState(() => _isFollowing = !_isFollowing);
    } else
      UtilService.notify(context: context, message: '${ _intl.translate('ANONYMOUS.LOGIN') }');
  }

  _disfollow({ required BuildContext context, required Users author }) async {
    if(widget.isDemo) {
      UtilService.notify(context: context, message: '${ _intl.translate("SCREEN.MEDIA.ALBUM.DETAILS.NOTIFICATION.DEMO") }');
      return;
    }

    if(!_authService.isAnonymous()) {
      await mainStore.user.disfollow(author: author);

      setState(() => _isFollowing = !_isFollowing);
    } else
      UtilService.notify(context: context, message: '${ _intl.translate('ANONYMOUS.LOGIN') }');
  }

  _seeProfile({ required BuildContext context, required Users author }) async {
    if(widget.isDemo) {
      UtilService.notify(context: context, message: '${ _intl.translate("SCREEN.MEDIA.ALBUM.DETAILS.NOTIFICATION.DEMO") }');
      return;
    }

    if(author.id != null) {
      final bool isCurrentUser = await _userService.isCurrentUser(author: author);

      if(!isCurrentUser)
        Navigator.of(context).push(UtilService.createRoute(ProfileDetails(isCurrentUser: false, displayUser: author)));
      else
        UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.MEDIA.ALBUM.DETAILS.NOTIFICATION.YOUR_OWN_PROFILE') }');
    }
  }

  _openContent({ required BuildContext context }) async {
    if(widget.media == null || widget.media.linkType == null || widget.media.linkId == null ||
        widget.media.linkType.isEmpty || widget.media.linkId.isEmpty) {
      return;
    }

    if(widget.media.type == MediaType.PODCAST) {
      //get podcast

      Navigator.push(context, UtilService.createRoute(
        MediaGroup(
          keyName: 'podcast_viewer',
          isHome: false,
          type: MediaScreenType.PODCAST,
          currentPosition: 0,
          isRecommend: false,
          medias: [ Media() ].toSet(),
        ),
      ));

    }
  }

  _openOriginalAudio({ required BuildContext context }) async {
    final MediaService _mediaService = MediaService();

    if(widget.isDemo) {
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.MEDIA.ALBUM.DETAILS.NOTIFICATION.DEMO') }');
      return;
    }

    final String originalAudioId = widget.media.originalMedia!.id!;
    final Media media = await _mediaService.getMediaById(id: originalAudioId);

    Navigator.of(context).push(
      UtilService.createRoute(
        MediaGroup(
          keyName: 'originalAudio',
          isHome: false,
          type: MediaScreenType.DEFAULT,
          currentPosition: 0,
          isRecommend: false,
          medias: [ media ].toSet(),
        ),
      ),
    );
  }

}
