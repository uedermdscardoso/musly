
import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/domain/model/utilities/loading_icon_type.dart';
import 'package:bridge/main.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'media_screen.dart';

class MediaGroup extends StatefulWidget {

  final String keyName;
  final Set<Media> medias;
  final num currentPosition;
  final MediaScreenType type;
  final bool isDemo;
  final bool isHome;
  final bool isRecommend;

  MediaGroup({Key? key,
    this.isDemo : false,
    this.type : MediaScreenType.DEFAULT,
    this.isHome : false,
    this.isRecommend : false,
    required this.keyName,
    required this.medias,
    this.currentPosition : 0,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MediaGroupState();
}

class _MediaGroupState extends State<MediaGroup> {

  ScrollController _controller = ScrollController();

  late MediaScreenType _type;

  double _totalHeight = 0;
  double _avgFirstHeight = 0;
  int _index = 0;
  int _currentIndex = 0;
  bool _isCurrent = false;
  bool _isUserPause = false;
  bool _isLoading = false;

  @override
  void initState() {
    _type = widget.type;
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    if(widget.medias.length > 0) {
      _totalHeight = MediaQuery.of(context).size.height * widget.medias.length;
      _avgFirstHeight = _totalHeight / widget.medias.length;
      _avgFirstHeight = _avgFirstHeight.isNaN ? 0 : _avgFirstHeight;
      _controller = ScrollController(initialScrollOffset: (_avgFirstHeight * (widget.currentPosition)).toDouble());
    }

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light,
        systemNavigationBarDividerColor: Colors.black,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: NotificationListener<OverscrollIndicatorNotification>(
              onNotification: (overscroll) {
                overscroll.disallowIndicator();

                return false;
              },
              child: NotificationListener<UserScrollNotification>(
                onNotification: (ScrollNotification scrollInfo) {
                  if(!_isLoading && scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
                    _loadMore();
                    setState(() {
                      _isLoading = true;
                    });
                  }

                  return false;
                },
                child: NotificationListener(
                  onNotification: (ScrollNotification notification) => _deslizar(notification: notification),
                  child: ListView.builder(
                    padding: const EdgeInsets.only(top: 0),
                    key: Key('${ widget.keyName }'),
                    physics: const PageScrollPhysics(),
                    //addAutomaticKeepAlives: true,
                    controller: _controller,
                    itemCount: widget.medias.length,
                    itemExtent: _avgFirstHeight,
                    cacheExtent: _avgFirstHeight,
                    itemBuilder: (context, position) {

                      final Media media = widget.medias.elementAt(position);

                      if(media == null || media.type == null)
                        return Container();

                      if(_index == position) {
                        _currentIndex = _index;
                        _isCurrent = true;
                      } else {
                        _isCurrent = false;
                      }

                      return MediaScreen(
                        type: widget.type,
                        keyName: widget.keyName,
                        isCurrent: _isCurrent,
                        //isUserPause: _isUserPause,
                        isDemo: widget.isDemo,
                        isHome: widget.isHome,
                        position: position,
                        media: media,
                      );
                    },
                  ),
                ),
              ),
            ),
          ),
          Container(
            color: Colors.black,
            height: _isLoading ? 175 : 0, //75
            child: Padding(
              padding: const EdgeInsets.only(bottom: 50),
              child: Loading(type: LoadingIconType.THREE_BALLS, message: ""),
            ),
          ),
        ],
      ),
    );
  }

  _deslizar({ required ScrollNotification notification }) {
    /*if(notification is ScrollStartNotification){
      setState(() {
        _isUserPause = true;
      });
    }*/

    if(notification is ScrollEndNotification && notification.metrics.axis == Axis.vertical) {
      double currentPosition = !notification.metrics.pixels.isInfinite ? notification.metrics.pixels : 0;

      setState(() {
        _index = (currentPosition /_avgFirstHeight).round();
      });

      //Remover o pause adicionado pelo usuário
      if(currentPosition.round() == (_avgFirstHeight * (_currentIndex + 1) - _avgFirstHeight).round()){ //false para index atual
        setState(() => _isUserPause = false);
      } else { //para index diferente do index atual
        if (currentPosition > (_avgFirstHeight * (_index + 1)) ||
            currentPosition < (_avgFirstHeight * (_index + 1))) {
          setState(() => _isUserPause = true);
        } else {
          setState(() => _isUserPause = false);
        }
      }
    }
    return false;
  }

  _loadMore() async {
    await new Future.delayed(new Duration(seconds: 5));

    setState(() {
      if(widget.isRecommend)
        mainStore.recommendation.loadMoreRecommendedMedias();
      else
        mainStore.recommendation.loadMoreFollowingMedias();

      _isLoading = false;
    });
  }

}
