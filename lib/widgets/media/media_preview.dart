
import 'dart:async';
import 'dart:io';

import 'package:bridge/domain/model/media/media_type.dart';
import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:flutter/material.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/image_choosing/image_choosing.dart';
import 'package:bridge/shared/modal_dialog.dart';
import 'package:bridge/widgets/media/media_group.dart';
import 'package:bridge/widgets/media/media_screen.dart';
import 'package:bridge/widgets/studio/media_link.dart';
import 'package:bridge/widgets/studio/media_publishing.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter/services.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/main.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/Translate.dart';

class MediaPreview extends StatefulWidget {

  final Set<Media> medias;

  const MediaPreview({Key? key,  required this.medias }) : super(key: key);

  @override
  _MediaPreviewState createState() => _MediaPreviewState();
}

class _MediaPreviewState extends State<MediaPreview> {

  late Translate _intl;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarDividerColor: Colors.black,
      ),
      child: Material(
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Color.fromRGBO(92, 92, 92, 1),
                Colors.black,
              ],
            ),
          ),
          child: Stack(
            fit: StackFit.expand,
            children: [
              MediaGroup(
                keyName: "preview",
                type: MediaScreenType.DEFAULT,
                isHome: false,
                medias: widget.medias,
                isDemo: true,
                isRecommend: false,
                currentPosition: 0,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Header(
                    left: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () => Navigator.pop(context),
                          child: Container(
                            child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                          ),
                        ),
                        Text('${ _intl.translate('SCREEN.STUDIO.PUBLISH.VIEWER.TITLE') }', style: TextStyle(color: Colors.grey, fontSize: 18, fontWeight: FontWeight.bold)),
                      ],
                    ),
                    right: GestureDetector(
                      onTap: () => _nextStep(context: context),
                      child: Container(
                        padding: EdgeInsets.only(top: 8, bottom: 8, right: 18, left: 18),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Text('Next', style: TextStyle(color: Colors.black)),
                      ),
                    ),
                    color: Colors.transparent,
                    hasBorder: false,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }


  _showDuetPreview() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Observer(
          builder: (_) {

            return ClipRRect(
              borderRadius: BorderRadius.circular(100), //18
              child: FutureBuilder(
                future: mainStore.imageChossing.tempImage,
                builder: (context, snapshot) {

                  if(snapshot.hasData) {
                    final File image = snapshot.data as File;

                    return Image.file(image, width: 100, height: 100);
                  }

                  return Image.asset('assets/images/thinker-dark.jpg', scale: 5.0);
                },
              ), //2.0
            );
          },
        ),
        const SizedBox(height: 25),
        Text('@voce', style: TextStyle(color: Colors.white, fontSize: 16)),
        const SizedBox(height: 18),
        Text('following', style: TextStyle(color: Colors.white, fontSize: 16)),
      ],
    );
  }

  Widget _showStitchedAudioPreview() {

    return MediaScreen(
      keyName: 'stitched_audio',
      isDemo: true,
      media: Media(), //widget.media,
      mediaType: MediaType.STITCHED_AUDIO,
      type: MediaScreenType.DEFAULT,
      isCurrent: true,
      isHome: false,
      position: 0,
    );

    /*return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Observer(
          builder: (_) {

            return ClipRRect(
              borderRadius: BorderRadius.circular(18),
              child: FutureBuilder(
                future: mainStore.imageChossing.tempImage,
                builder: (context, snapshot) {

                  if(snapshot.hasData) {
                    File image = snapshot.data;

                    return Image.file(image, width: 200, height: 200);
                  }

                  return Image.asset('assets/images/thinker-dark.jpg', scale: 2.5);
                },
              ), //2.0
            );
          },
        ),
        const SizedBox(height: 90), //60
        GestureDetector(
          onTap: () {
            if(_controller != null) {
              if (_controller.value.isPlaying) {
                _controller.pause();
              } else {
                _controller.play();
              }
            }

            setState(() => _showArrow = !_showArrow );
          },
          child: Container(
            width: 100, //75
            height: 100, //75
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(100),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Visibility(
                  visible: !_showArrow,
                  child: Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 6),
                      child: Image.asset('assets/icons/arrow.png',
                        scale: 1.2, //1.6,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: _showArrow,
                  child: Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 0),
                      child: Image.asset('assets/icons/pause_dark.png',
                        scale: 0.8, //1.0
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );*/
  }


  /*_showShortAudioPreview() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Observer(
          builder: (_) {

            return ClipRRect(
              borderRadius: BorderRadius.circular(18),
              child: FutureBuilder(
                future: mainStore.imageChossing.tempImage,
                builder: (context, snapshot) {

                  if(snapshot.hasData) {
                    File image = snapshot.data;

                    return Image.file(image, width: 200, height: 200);
                  }

                  return Image.asset('assets/images/thinker-dark.jpg', scale: 2.5);
                },
              ), //2.0
            );
          },
        ),
        const SizedBox(height: 90), //60
        GestureDetector(
          onTap: () {
            if(_controller != null) {
              if (_controller.value.isPlaying) {
                _controller.pause();
              } else {
                _controller.play();
              }
            }

            setState(() => _showArrow = !_showArrow );
          },
          child: Container(
            width: 100, //75
            height: 100, //75
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(100),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Visibility(
                  visible: !_showArrow,
                  child: Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 6),
                      child: Image.asset('assets/icons/arrow.png',
                        scale: 1.2, //1.6,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: _showArrow,
                  child: Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 0),
                      child: Image.asset('assets/icons/pause_dark.png',
                        scale: 0.8, //1.0
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }*/

  _editAudioImage({ required BuildContext context }) {
    showModalBottomSheet<void>( //loading
      context: context,
      elevation: 0,
      backgroundColor: Colors.transparent,
      barrierColor: Colors.white.withAlpha(1),
      isDismissible: true,
      builder: (BuildContext context) {
        return ImageChoosing();
      },
    );
  }

  _nextStep({ required BuildContext context }) async {
    showDialog<bool>(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.transparent,
        builder: (BuildContext context) {

          final String title = 'Do you want to link episode or audiobook?'; //_getClearModalMessage(menu: _menuSelected);

          return ModalDialog(
            title: title,
            positiveButtonName: 'Yes, I want to link episode or audiobook',
            negativeButtonName: 'No, I want to publish',
          );
        }
    ).then((isNo) {
      if(!(isNo ?? false))
        Navigator.of(context).push(UtilService.createRoute(MediaLink()));
      else
        Navigator.of(context).push(UtilService.createRoute(MediaPublishing(showModal: false)));
    });

  }

}
