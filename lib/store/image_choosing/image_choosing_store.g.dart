// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_choosing_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ImageChoosingStore on _ImageChoosingStore, Store {
  late final _$tempImageAtom =
      Atom(name: '_ImageChoosingStore.tempImage', context: context);

  @override
  Future<File>? get tempImage {
    _$tempImageAtom.reportRead();
    return super.tempImage;
  }

  @override
  set tempImage(Future<File>? value) {
    _$tempImageAtom.reportWrite(value, super.tempImage, () {
      super.tempImage = value;
    });
  }

  late final _$typeAtom =
      Atom(name: '_ImageChoosingStore.type', context: context);

  @override
  Future<FileUploaderType> get type {
    _$typeAtom.reportRead();
    return super.type;
  }

  @override
  set type(Future<FileUploaderType> value) {
    _$typeAtom.reportWrite(value, super.type, () {
      super.type = value;
    });
  }

  late final _$_ImageChoosingStoreActionController =
      ActionController(name: '_ImageChoosingStore', context: context);

  @override
  dynamic addTempImage({required File image}) {
    final _$actionInfo = _$_ImageChoosingStoreActionController.startAction(
        name: '_ImageChoosingStore.addTempImage');
    try {
      return super.addTempImage(image: image);
    } finally {
      _$_ImageChoosingStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic addFileUploaderType({required FileUploaderType type}) {
    final _$actionInfo = _$_ImageChoosingStoreActionController.startAction(
        name: '_ImageChoosingStore.addFileUploaderType');
    try {
      return super.addFileUploaderType(type: type);
    } finally {
      _$_ImageChoosingStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic clear() {
    final _$actionInfo = _$_ImageChoosingStoreActionController.startAction(
        name: '_ImageChoosingStore.clear');
    try {
      return super.clear();
    } finally {
      _$_ImageChoosingStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
tempImage: ${tempImage},
type: ${type}
    ''';
  }
}
