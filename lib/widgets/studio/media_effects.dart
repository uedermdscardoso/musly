
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/media/effect.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/effect/effect_service.dart';
import 'package:bridge/widgets/studio/effect_item.dart';

class MediaEffects extends StatefulWidget {
  @override
  _MediaEffectsState createState() => _MediaEffectsState();
}

class _MediaEffectsState extends State<MediaEffects> with AutomaticKeepAliveClientMixin {

  final _effectService = EffectService();

  List<Effect> _effects = [];

  @override
  void initState() {
    mainStore.effect.getEffects();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.black87,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(8), topRight: Radius.circular(8)),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 4, right: 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () async {
                    await mainStore.studio.clearEffect();
                    Navigator.pop(context);
                  },
                  child: Container(
                    child: Image.asset('assets/icons/none.png', scale: 1.80, color: Colors.white70),
                  ),
                ),
                Text('Effects', style: TextStyle(fontSize: 15, color: Colors.white)),
                Container(),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 18, left: 4, right: 4),
            child: StreamBuilder(
              stream: mainStore.effect.effects,
              builder: (context, snapshot) {

                //print(snapshot.hasData);

                switch(snapshot.connectionState) {
                  case ConnectionState.done: {} break;
                  case ConnectionState.none: {} break;
                  case ConnectionState.active: {

                    if(snapshot.hasData) {

                      final QuerySnapshot query = snapshot.data as QuerySnapshot;

                      return FutureBuilder(
                        future: _effectService.serializeList(snapshot: query),
                        builder: (_, content) {

                          switch(content.connectionState) {
                            case ConnectionState.none: {} break;
                            case ConnectionState.active: {} break;
                            case ConnectionState.done: {
                              if(content.hasData) {

                                _effects = content.data as List<Effect>;

                                return Container(
                                  height: 150,
                                  child: NotificationListener<OverscrollIndicatorNotification>(
                                    onNotification: (overscroll) {
                                      overscroll.disallowIndicator();

                                      return false;
                                    },
                                    child: GridView.count(
                                      crossAxisSpacing: 3, //2
                                      mainAxisSpacing: 3, //2
                                      crossAxisCount: 3, //7
                                      scrollDirection: Axis.horizontal,
                                      children: List.generate(_effects.length, (index) {
                                        final effect = _effects.elementAt(index);

                                        return GestureDetector(
                                          onTap: () {
                                            mainStore.studio.addEffect(effect: effect);
                                            Navigator.pop(context);
                                          },
                                          child: EffectItem(effect: effect),
                                        );
                                      }),
                                    ),
                                  ),
                                );
                              }
                            } break;
                            case ConnectionState.waiting: {
                              return Container(color: Colors.grey);
                            }
                          }

                          return Container();
                        },
                      );
                    }
                  } break;
                  case ConnectionState.waiting: {
                    return Container(color: Colors.green);
                  }
                }

                return Container();
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
