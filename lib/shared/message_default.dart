
import 'package:flutter/material.dart';

class MessageDefault extends StatelessWidget {

  String title;
  String subtitle;
  String textButton;
  GestureTapCallback onTap;
  Color? color;

  MessageDefault({
    Key? key,
    required this.title,
    required this.subtitle,
    required this.textButton,
    required this.onTap,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        color: color ?? Colors.black,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.only(right: 75, left: 75),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(child: Text('${ title }', textAlign: TextAlign.center, maxLines: 2, style: TextStyle(height: 1.25, color: Colors.white, fontSize: 20))),
                const SizedBox(height: 15),
                Flexible(child: Text('${ subtitle }', textAlign: TextAlign.center, style: TextStyle(height: 1.25, color: Colors.white70, fontSize: 15))),
                const SizedBox(height: 15),
                GestureDetector(
                  onTap: onTap,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(12),
                      child: Text('${ textButton }', textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 15, color: Colors.black, fontFamily: 'Arial', fontWeight: FontWeight.normal, decoration: TextDecoration.none)),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
