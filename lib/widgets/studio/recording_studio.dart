
import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:audioplayers/audioplayers.dart';
import 'package:bridge/domain/model/media/media_type.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sound/flutter_sound.dart' as FlutterSound;
import 'package:flutter_sound/public/flutter_sound_recorder.dart';
import 'package:bridge/domain/model/media/audio_dto.dart';
import 'package:bridge/domain/model/media/cutted_media_dto.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/domain/model/utilities/studio_menu_item_type.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/shared/image_choosing/image_choosing.dart';
import 'package:bridge/widgets/studio/creating_captions.dart';
import 'package:record/record.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:uuid/uuid.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:bridge/domain/model/media/effect.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/shared/skeleton_loading.dart';
import 'package:bridge/shared/modal_dialog.dart';
import 'package:bridge/widgets/studio/media_upload.dart';
import 'package:bridge/widgets/studio/media_effects.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/Translate.dart';

class RecordingStudio extends StatefulWidget {

  final MediaType type;

  const RecordingStudio({Key? key,  this.type : MediaType.SHORT_FORM_AUDIO }) : super(key: key);

  @override
  _RecordingStudioState createState() => _RecordingStudioState();
}

class _RecordingStudioState extends State<RecordingStudio> with TickerProviderStateMixin {

  final _IMAGE_ASSET = 'assets/images';

  final AuthenticationService _authService = AuthenticationService();
  final MediaService _mediaService = MediaService();
  final AudioPlayer _audioPlayer = AudioPlayer();

  final Record _record = Record();
  FlutterSoundRecorder _soundRecorder = FlutterSoundRecorder();

  final ScrollController _scrollCtrl = new ScrollController(initialScrollOffset: 0);
  late AnimationController _animateCtrl; //for text

  List<int> _shorts = [15, 30, 60]; //seconds
  List<double> _speed = [0.5, 1, 1.5, 2, 2.5]; //[0.3, 0.5, 1, 1.5, 2];

  StudioMenuItemType _itemType = StudioMenuItemType.NONE;
  bool _allowDuet = false;
  bool _isFollowing = false;
  bool _isRecording = false;
  bool _showPadSounds = false;

  int _limit = 0;
  int _defaultTime = 15000; //miliseconds
  double _defaultVolume = 100.0;
  double _defaultSpeed = 1.0;
  double offset = 0.0;

  late Timer _timer;
  Duration _duration = Duration(milliseconds: 0);
  String _hoursStr = '00';
  String _minutesStr = '00';
  String _secondsStr = '00';

  late Translate _intl;

  @override
  void initState() {
    _audioPlayer.setVolume(1.0);
    _animateText();
    _setInfo();
    super.initState();
  }

  @override
  void dispose() {
    mainStore.studio.dispose();
    mainStore.imageChossing.clear();
    _animateCtrl.dispose();
    super.dispose();
  }

  _setInfo() async {
    if(widget.type == MediaType.DUET) {
      final Media media = await mainStore.studio.media!;

      if(media.duration >= 15000) {
        if(media.duration >= 30000) {
          if(media.duration < 60000) {
            _defaultTime = 30000;
          }
        } else {
          _defaultTime = 30000;
        }
      } else {
        _defaultTime = 15000;
      }
    }
  }

  _animateText() {
    _animateCtrl = AnimationController(vsync: this, duration: Duration(seconds: 15))..addListener(() {
      if(_animateCtrl.isCompleted) _animateCtrl.repeat();
      offset += 1.0;
      if(offset - 1.0 > (_scrollCtrl.positions.isNotEmpty ? _scrollCtrl.offset : 0)) {
        offset = 0.0;
      }

      if(_scrollCtrl.positions.isNotEmpty) {
        setState(() => _scrollCtrl.jumpTo(offset));
      }
    });
    _animateCtrl.forward();
    _animateCtrl.repeat();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    _stopDuet(context: context);

    return WillPopScope(
      onWillPop: () async {
        return !_isRecording;
      },
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.light,
          statusBarIconBrightness: Brightness.light,
          systemNavigationBarIconBrightness: Brightness.light,
          systemNavigationBarColor: Colors.black,
          systemNavigationBarDividerColor: Colors.black,
        ),
        child: Material(
          child: Container(
            color: Colors.transparent,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [
                  Color.fromRGBO(92, 92, 92, 1),
                  Colors.black,
                ],
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Stack(
                fit: StackFit.expand,
                children: [
                  Observer(
                    builder: (_) {

                      return ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: FutureBuilder(
                          future: mainStore.imageChossing.tempImage,
                          builder: (context, snapshot) {

                            if(snapshot.hasData) {
                              File image = snapshot.data as File;

                              return Image.file(image, fit: BoxFit.cover);
                            }

                            return Image.asset('assets/images/anonymous.jpg', fit: BoxFit.cover);
                          },
                        ), //2.0
                      );
                    },
                  ),
                  Container(
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                          Color.fromRGBO(92, 92, 92, 1),
                          Colors.black87,
                        ],
                      ),
                    ),
                  ),
                  Observer(
                    builder: (_) {

                      _timer = mainStore.studio.timer!;

                      return Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(15),
                            child: Stack(
                              children: [
                                Visibility(
                                  visible: !_isRecording,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 12, right: 12),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: StreamBuilder<Object>(
                                        stream: null,
                                        builder: (context, snapshot) {
                                          return Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              GestureDetector(
                                                onTap: () => Navigator.pop(context),
                                                child: Container(
                                                  padding: const EdgeInsets.all(1),
                                                  child: Icon(Icons.close, size: 35, color: Colors.white),
                                                ), //arrow_back_ios
                                              ),
                                            ],
                                          );
                                        }
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 30,
                                  child: Column(
                                    children: [
                                      Visibility(
                                        visible: true, //_isRecording,
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: [
                                              FutureBuilder(
                                                future: mainStore.studio.cuttedMediaDTO,
                                                builder: (context, snapshot) {

                                                  if(snapshot.hasData) {

                                                    final CuttedMediaDTO cuttedMediaDTO = (snapshot.data) as CuttedMediaDTO;
                                                    final Duration duration = Duration(milliseconds: cuttedMediaDTO.duration);

                                                    return Container(
                                                      width: ((MediaQuery.of(context).size.width - 36) / _defaultTime) * duration.inMilliseconds,
                                                      height: 4,
                                                      decoration: BoxDecoration(
                                                        color: Colors.red[600], //Color.fromRGBO(255, 45, 0, 1)
                                                        borderRadius: BorderRadius.circular(100),
                                                      ),
                                                    );
                                                  }

                                                  return Container();
                                                },
                                              ),
                                              Expanded(
                                                child: Container(
                                                  height: 4,
                                                  decoration: BoxDecoration(
                                                    color: Colors.white10,
                                                    borderRadius: BorderRadius.circular(100),
                                                  ),
                                                  child: Row(
                                                    children: [
                                                      FutureBuilder(
                                                        future: mainStore.studio.cuttedMediaDTO,
                                                        builder: (context, snapshot) {

                                                          if(snapshot.hasData) {
                                                            final CuttedMediaDTO cuttedMediaDTO = snapshot.data as CuttedMediaDTO;

                                                            _duration = Duration(milliseconds: cuttedMediaDTO.duration);
                                                          }

                                                          return Container(
                                                            width: _timer != null ? (
                                                              (MediaQuery.of(context).size.width - 36) / _defaultTime) * _timer.tick : null,
                                                            height: 4,
                                                            decoration: BoxDecoration(
                                                              color: _isRecording ? Colors.grey : Colors.white10, //Color.fromRGBO(255, 45, 0, 1)
                                                              borderRadius: BorderRadius.circular(100),
                                                            ),
                                                          );
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      const SizedBox(height: 8),
                                      Visibility(
                                        visible: _isRecording,
                                        child: Row(
                                          children: [
                                            Container(
                                              width: 15,
                                              height: 15,
                                              decoration: BoxDecoration(
                                                color: Color.fromRGBO(255, 45, 0, 1),
                                                borderRadius: BorderRadius.circular(100),
                                              ),
                                            ),
                                            const SizedBox(width: 4),
                                            Text('REC', style: TextStyle(
                                                color: Color.fromRGBO(255, 45, 0, 1)),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Stack(
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    StreamBuilder(
                                      stream: Stream.value(_timer),
                                      builder: (context, snapshot) {

                                        if(_timer != null && snapshot.hasData) {
                                          _hoursStr = (((_timer.tick / 1000) / (60 * 60)) % 60)
                                              .floor()
                                              .toString()
                                              .padLeft(2, '0');

                                          _minutesStr = (((_timer.tick / 1000) / 60) % 60)
                                              .floor()
                                              .toString()
                                              .padLeft(2, '0');

                                          _secondsStr = ((_timer.tick / 1000) % 60).floor().toString().padLeft(2, '0');

                                          return Text('${ _hoursStr }:${ _minutesStr }:${ _secondsStr }', style: TextStyle(color: Colors.white, fontSize: 50));
                                        }

                                        return Text('00:00:00', style: TextStyle(color: Colors.white, fontSize: 50));
                                      },
                                    ),
                                    const SizedBox(height: 30),
                                    Container(
                                      width: 250,
                                      child: Text('${ _intl.translate('SCREEN.STUDIO.TIP.MESSAGE.FIRST') }',
                                        maxLines: 4,
                                        textAlign: TextAlign.center,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(height: 1.5, color: Colors.grey, fontSize: 12)),
                                    ),
                                    const SizedBox(height: 15),
                                    Container(
                                      width: 225,
                                      child: Text('${ _intl.translate('SCREEN.STUDIO.TIP.MESSAGE.SECOND') }',
                                        maxLines: 4,
                                        textAlign: TextAlign.center,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(height: 1.5, color: Colors.grey, fontSize: 12)),
                                    ),
                                    const SizedBox(height: 30),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Visibility(
                                              visible: !_isRecording || !_showPadSounds,
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Visibility(
                                                    visible: widget.type == MediaType.DUET,
                                                    child: Padding(
                                                      padding: const EdgeInsets.only(top: 40, left: 12, right: 20),
                                                      child: Column(
                                                        children: [
                                                          FutureBuilder(
                                                            future: mainStore.studio.media,
                                                            builder: (context, snapshot) {

                                                              if(snapshot.hasData) {

                                                                final Media media = snapshot.data as Media;

                                                                return Row(
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  children: [
                                                                    ClipRRect(
                                                                      borderRadius: BorderRadius.circular(100),
                                                                      child: ColorFiltered(
                                                                        colorFilter: const ColorFilter.mode(
                                                                          Colors.grey,
                                                                          BlendMode.saturation,
                                                                        ),
                                                                        child: CachedNetworkImage(
                                                                          imageUrl: media.createdBy!.photo,
                                                                          width: 62,
                                                                          height: 62,
                                                                          fit: BoxFit.cover,
                                                                          placeholder: (context, url) => Image.asset('${ _IMAGE_ASSET }/bird-dark.jpg', scale: 5),
                                                                          errorWidget: (context, url, error) => Image.asset('${ _IMAGE_ASSET }/bird-dark.jpg', scale: 5),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    const SizedBox(width: 12),
                                                                    Column(
                                                                      children: [
                                                                        Text('${ media.createdBy!.firstName } ${ media.createdBy!.lastName }',
                                                                            style: TextStyle(fontSize: 14, color: Colors.white)),
                                                                        const SizedBox(height: 8),
                                                                        Text('@${ media.createdBy!.username }',
                                                                            style: TextStyle(fontSize: 14, color: Colors.grey[300])),
                                                                      ],
                                                                    ),
                                                                    const SizedBox(width: 12),
                                                                    Container(
                                                                      padding: EdgeInsets.all(3),
                                                                      decoration: BoxDecoration(
                                                                        border: Border.all(color: Colors.white, width: 0.5),
                                                                        borderRadius: BorderRadius.circular(100),
                                                                      ),
                                                                      child: StreamBuilder(
                                                                        stream: _audioPlayer.onPlayerStateChanged,
                                                                        builder: (context, snapshot) {

                                                                          if(snapshot.hasData) {
                                                                            final PlayerState state = snapshot.data as PlayerState;
                                                                            final bool isPlaying = state == PlayerState.playing;

                                                                            return Visibility(
                                                                              visible: isPlaying,
                                                                              child: Icon(Icons.pause, color: Colors.white, size: 20),
                                                                              replacement: Icon(Icons.play_arrow, color: Colors.white, size: 20),
                                                                            );
                                                                          }

                                                                          return Icon(Icons.play_arrow, color: Colors.white, size: 20);
                                                                        },
                                                                      ),
                                                                    ),
                                                                  ],
                                                                );
                                                              }

                                                              return Container();
                                                            },
                                                          ),
                                                          const SizedBox(height: 25),
                                                          FutureBuilder(
                                                            future: Future.value(mainStore.auth.currentUser),
                                                            builder: (context, snapshot) {

                                                              if(snapshot.hasData) {

                                                                final Users currentUser = snapshot.data as Users;

                                                                return Row(
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  children: [
                                                                    ClipRRect(
                                                                      borderRadius: BorderRadius.circular(100),
                                                                      child: ColorFiltered(
                                                                        colorFilter: const ColorFilter.mode(
                                                                          Colors.grey,
                                                                          BlendMode.saturation,
                                                                        ),
                                                                        child: CachedNetworkImage(
                                                                          imageUrl: currentUser.photo,
                                                                          width: 62,
                                                                          height: 62,
                                                                          fit: BoxFit.cover,
                                                                          placeholder: (context, url) => Image.asset('${ _IMAGE_ASSET }/bird-dark.jpg', scale: 5),
                                                                          errorWidget: (context, url, error) => Image.asset('${ _IMAGE_ASSET }/bird-dark.jpg', scale: 5),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    const SizedBox(width: 12),
                                                                    Column(
                                                                      children: [
                                                                        Text('${ currentUser.firstName } ${ currentUser.lastName }',
                                                                            style: TextStyle(fontSize: 14, color: Colors.white)),
                                                                        const SizedBox(height: 8),
                                                                        Text('@${ currentUser != null ? currentUser.username : '' }',
                                                                            style: TextStyle(fontSize: 14, color: Colors.grey[300])),
                                                                      ],
                                                                    ),
                                                                    const SizedBox(width: 12),
                                                                    Container(
                                                                      padding: EdgeInsets.all(8),
                                                                      decoration: BoxDecoration(
                                                                        border: Border.all(color: Colors.white, width: 0.5),
                                                                        borderRadius: BorderRadius.circular(100),
                                                                      ),
                                                                      child: Visibility(
                                                                        visible: _audioPlayer.state != PlayerState.playing,
                                                                        child: Container(
                                                                          color: Colors.white,
                                                                          width: 10,
                                                                          height: 10,
                                                                        ),
                                                                        replacement: Container(
                                                                          width: 10,
                                                                          height: 10,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                );
                                                              }

                                                              return Container();
                                                            },
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            SizedBox(height: MediaQuery.of(context).size.height * 0.075),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: Align(
                                    alignment: Alignment.topRight,
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 75),
                                      child: Visibility(
                                        visible: _itemType == StudioMenuItemType.VOLUME,
                                        child: Padding(
                                          padding: const EdgeInsets.only(top: 75, right: 65),
                                          child: Container(
                                            width: 50,
                                            height: 250,
                                            decoration: BoxDecoration(
                                              color: Colors.grey[400],
                                              borderRadius: BorderRadius.circular(8),
                                            ),
                                            child: Listener(
                                              onPointerUp: (PointerUpEvent event) {
                                                setState(() => _itemType = StudioMenuItemType.NONE);
                                              },
                                              child: SfSlider.vertical(
                                                value: _defaultVolume,
                                                min: 0.0,
                                                max: 100.0,
                                                labelPlacement: LabelPlacement.betweenTicks,
                                                activeColor: Colors.grey[600],
                                                inactiveColor: Colors.grey[500],
                                                showTicks: true,
                                                showLabels: false,
                                                enableTooltip: true,
                                                interval: 25,
                                                minorTicksPerInterval: 1,
                                                onChanged: (dynamic value) {
                                                  setState(() => _defaultVolume = value);
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                        replacement: Visibility(
                                          visible: _itemType == StudioMenuItemType.SPEED,
                                          child: Padding(
                                            padding: const EdgeInsets.only(top: 82, right: 65),
                                            child: Container(
                                              width: 50,
                                              height: 250,
                                              decoration: BoxDecoration(
                                                color: Colors.grey[400],
                                                borderRadius: BorderRadius.circular(8),
                                              ),
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: List<Widget>.generate(_speed.length, (index) {

                                                  final bool isSpeedSelected = _defaultSpeed.toString().contains(_speed.elementAt(index).toString());

                                                  return Padding(
                                                    padding: const EdgeInsets.only(bottom: 4, left: 4, right: 4),
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        if(!_isRecording) {
                                                          setState(() {
                                                            _defaultSpeed = double.parse(_speed.elementAt(index).toString());
                                                            _itemType = StudioMenuItemType.NONE;
                                                          });
                                                        }
                                                      },
                                                      child: Container(
                                                        width: 60,
                                                        height: 42,
                                                        decoration: BoxDecoration(
                                                          color: isSpeedSelected ? Colors.grey[800] : Colors.white10,
                                                          border: Border.all(color: !isSpeedSelected ? Colors.white12 : Colors.grey,
                                                              width: isSpeedSelected ? 0.75 : 0.50), //0.50
                                                          borderRadius: BorderRadius.circular(6),
                                                        ),
                                                        child: Center(child: Text('${ _speed.elementAt(index) }x',
                                                          style: TextStyle(color: isSpeedSelected ? Colors.white : Colors.grey[600]),
                                                        )),
                                                      ),
                                                    ),
                                                  );
                                                }),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 20, right: 20),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Visibility(
                                      visible: !_isRecording,
                                      child: Container(),
                                    ),
                                    //record button
                                    Observer(
                                      builder: (_) {

                                        return Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            //close
                                            FutureBuilder(
                                              future: mainStore.studio.audioToSend,
                                              builder: (context, snapshot) {

                                                if(snapshot.hasData) {

                                                  final Media audio = snapshot.data as Media;

                                                  return Visibility(
                                                    visible: audio != null && !_isRecording,
                                                    child: GestureDetector(
                                                      onTap: () => _showAudioDiscardDialog(context: context),
                                                      child: Container(
                                                        padding: EdgeInsets.all(6),
                                                        decoration: BoxDecoration(
                                                          color: Colors.grey[100],
                                                          borderRadius: BorderRadius.circular(6),
                                                        ),
                                                        child: Icon(Icons.close, size: 18, color: Colors.black),
                                                      ),
                                                    ),
                                                  );
                                                }

                                                return Container();
                                              },
                                            ),
                                            SizedBox(width: MediaQuery.of(context).size.width * 0.045), //18
                                            //record button
                                            GestureDetector(
                                              onTap: () {
                                                if(!_isRecording) {
                                                  if(widget.type != MediaType.DUET) {
                                                    _startRecording(context: context);
                                                  } else {
                                                    _startDuet(context: context); // allow duet
                                                    setState(() => this._allowDuet = true);
                                                  }
                                                } else {
                                                  _stopRecording(context: context);
                                                }
                                              },
                                              child: Container(
                                                width: 75,
                                                height: 75,
                                                child: Stack(
                                                  children: [
                                                    Container(
                                                      decoration: BoxDecoration(
                                                        color: !_isRecording ? Colors.red[800] : Color.fromRGBO(255, 45, 0, 1),
                                                        borderRadius: BorderRadius.circular(100),
                                                      ),
                                                    ),
                                                    Center(
                                                      child: Container(
                                                        width: 70,
                                                        height: 70,
                                                        decoration: BoxDecoration(
                                                          color: Colors.black,
                                                          borderRadius: BorderRadius.circular(100),
                                                        ),
                                                      ),
                                                    ),
                                                    if(!_isRecording) Center(
                                                      child: Container(
                                                        width: 65,
                                                        height: 65,
                                                        decoration: BoxDecoration(
                                                          //color: Color.fromRGBO(255, 45, 0, 1),
                                                          gradient: const LinearGradient(
                                                            begin: Alignment.topRight,
                                                            end: Alignment.bottomLeft,
                                                            colors: [
                                                              Color.fromRGBO(255, 45, 0, 1),
                                                              Color.fromRGBO(138, 0, 0, 1),
                                                            ],
                                                          ),
                                                          borderRadius: BorderRadius.circular(100),
                                                        ),
                                                        child: Image.asset("assets/icons/icon_mic.png", color: Colors.white, scale: 1.10),
                                                      ),
                                                    ) else Center(
                                                      child: Container(
                                                        width: 30,
                                                        height: 30,
                                                        decoration: BoxDecoration(
                                                          gradient: const LinearGradient(
                                                            begin: Alignment.topRight,
                                                            end: Alignment.bottomLeft,
                                                            colors: [
                                                              Color.fromRGBO(255, 45, 0, 1),
                                                              Color.fromRGBO(138, 0, 0, 1),
                                                            ],
                                                          ),
                                                          borderRadius: BorderRadius.circular(4),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            SizedBox(width: MediaQuery.of(context).size.width * 0.045), //18
                                            //check
                                            FutureBuilder(
                                              future: mainStore.studio.audioToSend,
                                              builder: (context, snapshot) {

                                                if(snapshot.hasData) {

                                                  final Media audio = snapshot.data as Media;

                                                  return Visibility(
                                                    visible: audio != null && mainStore.studio.audioToSend != null && !_isRecording,
                                                    child: GestureDetector(
                                                      onTap: () async {
                                                        final Media newMedia = (await mainStore.studio.audioToSend)!;

                                                        Navigator.of(context).push(UtilService.createRoute(CreatingCaptions(media: newMedia)));
                                                      },
                                                      child: Container(
                                                        padding: EdgeInsets.all(6),
                                                        decoration: BoxDecoration(
                                                          color: Colors.pink, //green,
                                                          borderRadius: BorderRadius.circular(100),
                                                        ),
                                                        child: Icon(Icons.check, size: 18, color: Colors.white),
                                                      ),
                                                    ),
                                                  );
                                                }

                                                return Container();
                                              },
                                            ),
                                          ],
                                        );
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(height: 6),
                              /*Container(
                                padding: EdgeInsets.all(12),
                                color: Colors.black,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: List<Widget>.generate(_shorts.length, (index) {

                                    int time = _shorts.elementAt(index);
                                    double timeSelected = (_defaultTime ?? 0) / 1000;
                                    bool isTimeSelected = timeSelected == time.toDouble();

                                    return Padding(
                                      padding: const EdgeInsets.only(left: 4, right: 4),
                                      child: GestureDetector(
                                        onTap: () {
                                          if(!_isRecording) {
                                            final int seconds = _shorts.elementAt(index) * 1000;
                                            setState(() => _defaultTime = seconds);
                                          }
                                        },
                                        child: Container(
                                          width: 40,
                                          height: 32,
                                          decoration: BoxDecoration(
                                            color: Colors.black,
                                            border: Border.all(color: !isTimeSelected ? Colors.white12 : Colors.grey, width: isTimeSelected ? 0.40 : 0.50), //0.50
                                            borderRadius: BorderRadius.circular(6),
                                          ),
                                          child: Center(child: Text('${ time }',
                                            style: TextStyle(color: isTimeSelected ? Colors.grey[300] : Colors.grey ),
                                          )),
                                        ),
                                      ),
                                    );
                                  }),
                                ),
                              ),*/
                              Visibility(
                                visible: !_isRecording,
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 34, left: 4, right: 4, bottom: 6),
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        GestureDetector(
                                          onTap: () => showModalBottomSheet<void>( //loading
                                            context: context,
                                            elevation: 0,
                                            backgroundColor: Colors.transparent,
                                            barrierColor: Colors.white.withAlpha(0),
                                            isDismissible: true,
                                            builder: (BuildContext context) {

                                              return ImageChoosing();
                                            },
                                          ),
                                          child: Container(
                                            padding: EdgeInsets.all(0),
                                            width: 75,
                                            height: 75,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(14),
                                              border: Border.all(color: Colors.grey, width: 0.5),
                                            ),
                                            child: Stack(
                                              fit: StackFit.expand,
                                              children: [
                                                Observer(
                                                  builder: (_) {

                                                    return ClipRRect(
                                                      borderRadius: BorderRadius.circular(14),
                                                      child: BackdropFilter(
                                                        filter: ImageFilter.blur(
                                                          sigmaX: 10.0,
                                                          sigmaY: 10.0,
                                                        ),
                                                        child: Container(
                                                          color: Colors.grey.withOpacity(0.5),
                                                          child: FutureBuilder(
                                                            future: mainStore.imageChossing.tempImage,
                                                            builder: (context, snapshot) {

                                                              if(snapshot.hasData) {
                                                                final File image = snapshot.data as File;

                                                                return Image.file(image, fit: BoxFit.cover);
                                                              }

                                                              return Image.asset('assets/images/anonymous.jpg', fit: BoxFit.cover);
                                                            },
                                                          ),
                                                        ),
                                                      ), //1.0
                                                    );
                                                  },
                                                ),
                                                Visibility(
                                                  visible: !_isRecording,
                                                  child: Column(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: const [
                                                      Icon(Icons.edit, color: Colors.white, size: 28),
                                                      SizedBox(height: 8),
                                                      Text("Background",
                                                        overflow: TextOverflow.ellipsis,
                                                        maxLines: 2,
                                                        textAlign: TextAlign.center,
                                                        style: TextStyle(color: Colors.white, fontSize: 10),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        const SizedBox(width: 12),
                                        Container(
                                          padding: EdgeInsets.all(12),
                                          width: 75,
                                          height: 75,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(14),
                                            border: Border.all(color: Colors.grey, width: 0.5),
                                          ),
                                          child: GestureDetector(
                                            onTap: () {
                                              if(_itemType == StudioMenuItemType.NONE) {
                                                setState(() => _itemType = StudioMenuItemType.SPEED);
                                              } else {
                                                setState(() => _itemType = StudioMenuItemType.NONE);
                                              }
                                            },
                                            child: Column(
                                              children: const [
                                                Icon(Icons.speed, color: Colors.white, size: 24),
                                                SizedBox(height: 11),
                                                Text('Speed', style: TextStyle(color: Colors.white, fontSize: 11)),
                                              ],
                                            ),
                                          ),
                                        ),
                                        const SizedBox(width: 12),
                                        GestureDetector(
                                          onTap: () => showModalBottomSheet<void>( //loading
                                            context: context,
                                            elevation: -1,
                                            backgroundColor: Colors.transparent,
                                            barrierColor: Colors.white.withAlpha(0),
                                            isDismissible: true,
                                            builder: (BuildContext context) {
                                              return MediaEffects();
                                            },
                                          ),
                                          child: Observer(
                                            builder: (_) {
                                              return Container(
                                                width: 75,
                                                height: 75,
                                                child: FutureBuilder(
                                                  future: mainStore.studio.effect,
                                                  builder: (context, snapshot) {
                                                    switch(snapshot.connectionState) {
                                                      case ConnectionState.none: {} break;
                                                      case ConnectionState.active: {} break;
                                                      case ConnectionState.done: {

                                                        if(snapshot.hasData) {
                                                          final Effect effect = snapshot.data as Effect;

                                                          return Container(
                                                            padding: EdgeInsets.all(12),
                                                            decoration: BoxDecoration(
                                                              color: Colors.grey[799],
                                                              borderRadius: BorderRadius.circular(7.0),
                                                            ),
                                                            child: Column(
                                                              children: [
                                                                CachedNetworkImage(
                                                                  imageUrl: effect.image ?? '',
                                                                  width: 24,
                                                                  height: 24,
                                                                  placeholder: (context, url) => SkeletonLoading(width: 24, height: 24),
                                                                  errorWidget: (context, url, error) => Image.asset('assets/icons/no_image.png', scale: 4, color: Colors.white), //Image.asset('assets/icons/no_image.png', scale: 5, color: Colors.white),
                                                                ),
                                                                const SizedBox(height: 7),
                                                                Text('${ effect.name }',
                                                                    textAlign: TextAlign.center,
                                                                    overflow: TextOverflow.ellipsis,
                                                                    maxLines: 1,
                                                                    style: TextStyle(color: Colors.white, fontSize: 9)),
                                                              ],
                                                            ),
                                                          );
                                                        }
                                                      } break;
                                                      case ConnectionState.waiting: {
                                                        return Container();
                                                      } break;
                                                    }

                                                    return Container(
                                                      padding: const EdgeInsets.all(12),
                                                      decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(14),
                                                        border: Border.all(color: Colors.grey, width: 0.5),
                                                      ),
                                                      child: Column(
                                                        children: [
                                                          Image.asset('assets/icons/effect.png', color: Colors.white, width: 25, height: 25),
                                                          const SizedBox(height: 12),
                                                          Text('${ _intl.translate('SCREEN.STUDIO.BUTTON.EFFECT') }', style: TextStyle(color: Colors.white, fontSize: 10)),
                                                        ],
                                                      ),
                                                    );
                                                  },
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                        /*const SizedBox(width: 12),
                                        GestureDetector(
                                          onTap: () => Navigator.of(context).push(UtilService.createRoute(BackgroundSoundSelection())),
                                          child: Container(
                                            width: 75,
                                            height: 75,
                                            padding: const EdgeInsets.all(12),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(14),
                                              border: Border.all(color: Colors.grey[600], width: 0.5),
                                            ),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Icon(Icons.close, color: Colors.white, size: 12),
                                                const SizedBox(width: 4),
                                                FutureBuilder(
                                                  future: mainStore.studio.backgroundSound,
                                                  builder: (context, snapshot) {

                                                    if(snapshot.hasData) {

                                                      final Media sound = snapshot.data;

                                                      if(sound == null)
                                                        _animateCtrl.stop();
                                                      else
                                                      if(!_animateCtrl.isAnimating)
                                                        _animateCtrl.repeat();

                                                      return AnimatedText(
                                                        width: MediaQuery.of(context).size.width * 0.22, //0.60
                                                        alignment: MainAxisAlignment.center,
                                                        text: sound != null ? sound.title : 'music',
                                                        direction: Axis.horizontal,
                                                        scrollCtrl: _scrollCtrl,
                                                      );

                                                      //return Text('${ sound.name }', style: TextStyle(color: Colors.white, fontSize: 15));
                                                    }
                                                    return Text('music', style: TextStyle(color: Colors.white, fontSize: 15));
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),*/
                                        const SizedBox(width: 12),
                                        Visibility(
                                          visible: !_isRecording,
                                          child: Visibility(
                                            visible: widget.type != MediaType.DUET,
                                            child: GestureDetector(
                                              onTap: () => Navigator.of(context).push(UtilService.createRoute(MediaUpload())),
                                              child: Container(
                                                width: 75,
                                                height: 75,
                                                child: Container(
                                                  padding: const EdgeInsets.all(12),
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(14),
                                                    border: Border.all(color: Colors.grey[600]!, width: 0.5),
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      Image.asset('assets/icons/upload.png', color: Colors.grey[200], width: 25, height: 22),
                                                      const SizedBox(height: 12),
                                                      Text('${ _intl.translate('SCREEN.STUDIO.BUTTON.UPLOAD') }', style: TextStyle(color: Colors.white, fontSize: 10)),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _startDuet({ required BuildContext context }) async {
    final Media mediaForDuet = await mainStore.studio.media!;

    if(_allowDuet && mediaForDuet != null) {
      _limit = _defaultTime;
      _audioPlayer.play(new AssetSource(mediaForDuet.path));
      _audioPlayer.setVolume(1.0);
      setState(() => _isRecording = true);
      _controlTimer(context: context);
    }
  }

  _stopDuet({ required BuildContext context }) async {
    final Media media = await mainStore.studio.media!;
    Stream<Duration> stream = _audioPlayer.onPositionChanged;

    stream.listen((event) async {
      final int currentPosition = await event.inMilliseconds;

      if(currentPosition >= media.duration) {
        _audioPlayer.stop();
        _startRecording(context: context);
      }
    });
  }

  _startRecording({ required BuildContext context }) async {
    final Effect effect = await mainStore.studio.effect!;

    if(effect == null || !(effect.unavailable ?? true)) {

      final bool hasPermission = await _record.hasPermission(); //await _record.hasPermission();

      if(hasPermission) {

        _limit = _defaultTime;

        final CuttedMediaDTO cuttedMediaDTO = await (mainStore.studio.cuttedMediaDTO)!;

        if(widget.type == MediaType.STITCHED_AUDIO) {
          if(!this._allowDuet) {
            _limit -= cuttedMediaDTO.duration;
          } else {
            _limit -= cuttedMediaDTO.duration;
          }
        }

        final String mediaName = Uuid().v4();
        final String mediaPath = join((await getTemporaryDirectory()).path, '${ mediaName }.aac');

        _soundRecorder.openRecorder().then((value) {
          _soundRecorder.startRecorder(
            codec: FlutterSound.Codec.aacADTS,
            sampleRate: 44100,
            toFile: mediaPath,
          );
        });

        /*await _record.start(
          path: '${ mediaPath }',
          encoder: AudioEncoder.AAC,
          bitRate: 128000,
          samplingRate: 44100,
        ).catchError((e) {
          throw Exception(e);
        });*/

        if(!this._allowDuet) {
          _controlTimer(context: context);
        }

        setState(() => _isRecording = true );
      }
    } else {
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.STUDIO.NOTIFICATION.EFFECT_UNAVAILABLE') }');
    }
  }

  _stopRecording({ required BuildContext context }) async {

    //final Recording current = await _recorder.current(channel: 0);

    if(await _soundRecorder.isRecording) {
    //if(await current.status == RecordingStatus.Recording) {
    //if(await _record.isRecording()) {

      final String mediaPath = (await _soundRecorder.stopRecorder())!;
      _soundRecorder.closeRecorder();

      //final String mediaPath = await _record.stop();
      //Recording recording = await _recorder.stop();
      //String mediaPath = recording.path;

      _stopTimer();
      _audioPlayer.stop();

      final CuttedMediaDTO cuttedMediaDTO = (await mainStore.studio.cuttedMediaDTO)!;
      int miliseconds = _defaultTime;

      if(widget.type == MediaType.STITCHED_AUDIO)
        miliseconds -= cuttedMediaDTO.duration;

      final double seconds = (miliseconds / 1000);

      if(seconds > 1) {

        final AudioDTO _recordedAudioDTO = AudioDTO(audio: File(mediaPath));
        //_recordedAudio = File(mediaPath);

        if(await _recordedAudioDTO.audio.exists()) {
          final AuthenticationService authService = AuthenticationService();

          if(!authService.isAnonymous()) {
            final currentUser = await authService.getCurrentUser();

            final File audioImage = await mainStore.imageChossing.tempImage!;
            final Effect effect = await mainStore.studio.effect!;

            _recordedAudioDTO.applyEffect(effect: effect);
            _recordedAudioDTO.applySpeed(speed: _defaultSpeed);
            _recordedAudioDTO.stitchAudio(clipped: cuttedMediaDTO.audio!);

            final File newAudio = _recordedAudioDTO.audio;

            if(newAudio != null && newAudio.existsSync()) {
              final String id = Uuid().v4().replaceAll('-', '');
              final int duration = await _mediaService.getDuration(path: newAudio.path);

              final Media newMedia = Media.forNewMedia(
                id: id,
                filename: '${ id }.aac', //.aac
                duration: duration,
                createdBy: currentUser,
                mediaFile: newAudio,
                imageFile: audioImage,
                originalMedia: cuttedMediaDTO,
              );

              await mainStore.studio.addAudioToSend(audio: newMedia);

              Navigator.of(context).push(UtilService.createRoute(CreatingCaptions(media: newMedia)));
            }
          }
        }
      } else {
        UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.STUDIO.NOTIFICATION.TIME_INVALID') }');
      }

      setState(() {
        _isRecording = false;
        _defaultTime = 15000;
      });
    }
  }

  _controlTimer({ required BuildContext context }) async {
    if(_limit > 0) {
      var oneSec = Duration(milliseconds: 1); //microseconds: 1000

      await mainStore.studio.addTimer(timer: Timer.periodic(
        oneSec, (Timer timer) {
          if(mounted) {
            setState(() {
              if(timer.tick >= _limit+100) {
                timer.cancel();
                _stopRecording(context: context);
                _isRecording = false;
              }
            });
          }
        }),
      );
    }
  }

  _showAudioDiscardDialog({ required BuildContext context }) async {
    await showDialog<bool>(
      context: context,
      barrierDismissible: true,
      barrierColor: Colors.white.withAlpha(1),
      builder: (BuildContext context) {
        return ModalDialog(
          title: '${ _intl.translate('SCREEN.STUDIO.MODAL.DISCARD.TITLE') }',
          positiveButtonName: '${ _intl.translate('SCREEN.STUDIO.MODAL.DISCARD.ACTION.KEEP') }',
          negativeButtonName: '${ _intl.translate('SCREEN.STUDIO.MODAL.DISCARD.ACTION.DISCARD') }',
        );
      },
    ).then((isDiscarded) {
      if(isDiscarded ?? false) {
        _clearAudio();
      }
    });
  }

  _stopTimer() async {
    if(mainStore.studio.timer != null) {
      setState(() => mainStore.studio.clearTimer());
    }
  }

  _clearAudio() async {
    await mainStore.studio.clearAudioToSend();
  }

  Future<File> _stitchAudio({ required File clippedMedia, required File changedAudio }) async {
    if(widget.type == MediaType.STITCHED_AUDIO) {
      return await _mediaService.concatTwoAudios(input1: clippedMedia, input2: changedAudio);
    }

    return changedAudio;
  }

  Future<File> _applySpeed({ required double defaultSpeed, required File changedAudio }) async {
    if(defaultSpeed > 1.0 || defaultSpeed < 1.0) {
      return await _mediaService.changeSpeed(speed: defaultSpeed, audio: changedAudio);
    }

    return changedAudio;
  }

  Future<File> _applyEffect({ required Effect effect, required File audio }) async {
    if(effect != null && audio != null && audio.existsSync()) {
      return await _mediaService.addEffect(syntax: effect.syntax ?? '', audio: audio);
    }

    return audio;
  }

  Future<File> _applyVolume({ required double volume, required File backgroundSound }) async {
    if(volume > -1) {
      return await _mediaService.applyVolume(filepath: backgroundSound.path, volume: volume);
    }

    return backgroundSound;
  }

  Future<File> _mergeAudios({ required double seconds, required double defaultTime, required File background, required File recordedAudio }) async {
    File mergedAudio;

    if(seconds <= defaultTime) {
      final File cutedSoundBackground = await _mediaService.cutAudio(
          seconds: seconds, path: background.path);

      mergedAudio = await _mediaService.mergeAudios(
          first: cutedSoundBackground, second: recordedAudio);
    } else {
      final File cutedRecordedAudio = await _mediaService.cutAudio(
          seconds: defaultTime, path: recordedAudio.path);

      mergedAudio = await _mediaService.mergeAudios(
          first: background, //background sound
          second: cutedRecordedAudio);
    }

    return mergedAudio;
  }

  _follow({ required BuildContext context }) async {
    if(!_authService.isAnonymous()) {
      final Media media = await mainStore.studio.media!;

      await mainStore.user.follow(author: media.createdBy!);

      setState(() => _isFollowing = !_isFollowing);
    } else {
      UtilService.notify(context: context, message: '${ _intl.translate('ANONYMOUS.LOGIN') }');
    }
  }

  _disfollow({ required BuildContext context }) async {
    if(!_authService.isAnonymous()) {
      final Media media = await mainStore.studio.media!;

      await mainStore.user.disfollow(author: media.createdBy!);

      setState(() => _isFollowing = !_isFollowing);
    } else {
      UtilService.notify(context: context, message: '${ _intl.translate('ANONYMOUS.LOGIN') }');
    }
  }

}
