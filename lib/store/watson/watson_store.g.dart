// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'watson_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$WatsonStore on _WatsonStore, Store {
  late final _$captionsAtom =
      Atom(name: '_WatsonStore.captions', context: context);

  @override
  Future<List<CustomCaption>>? get captions {
    _$captionsAtom.reportRead();
    return super.captions;
  }

  @override
  set captions(Future<List<CustomCaption>>? value) {
    _$captionsAtom.reportWrite(value, super.captions, () {
      super.captions = value;
    });
  }

  late final _$_WatsonStoreActionController =
      ActionController(name: '_WatsonStore', context: context);

  @override
  dynamic addCaptions({required List<CustomCaption> captions}) {
    final _$actionInfo = _$_WatsonStoreActionController.startAction(
        name: '_WatsonStore.addCaptions');
    try {
      return super.addCaptions(captions: captions);
    } finally {
      _$_WatsonStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic removeCaptions() {
    final _$actionInfo = _$_WatsonStoreActionController.startAction(
        name: '_WatsonStore.removeCaptions');
    try {
      return super.removeCaptions();
    } finally {
      _$_WatsonStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
captions: ${captions}
    ''';
  }
}
