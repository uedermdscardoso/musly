
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/main.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/widgets/header.dart';

class MetadataConfig extends StatefulWidget {
  const MetadataConfig({ Key? key }) : super(key: key);

  @override
  _MetadataConfigState createState() => _MetadataConfigState();
}

class _MetadataConfigState extends State<MetadataConfig> {

  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _authorController = TextEditingController();

  bool _isAuthor = true;

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init() async {
    _isAuthor = (await mainStore.studio.isAuthor) ?? true;
    _titleController.text = await mainStore.studio.title!;
    _authorController.text = await mainStore.studio.author!;
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Header(
              left: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                    ),
                  ),
                  Text('Metadata', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
                ],
              ),
              right: GestureDetector(
                onTap: () => _saveMetadata(context: context),
                child: Container(
                  padding: EdgeInsets.only(top: 8, bottom: 8, right: 18, left: 18),
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Text('Save', style: TextStyle(color: Colors.white)),
                ),
              ),
              color: Colors.transparent,
              hasBorder: true,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 30, bottom: 25, left: 18, right: 18),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    CustomField(
                      controller: _titleController,
                      autofocus: false,
                      textColor: Colors.grey,
                      borderColor: Colors.grey[300],
                      backgroundColor: Colors.white,
                      maxLines: 1,
                      style: TextStyle(height: 1.5),
                      contentPadding: EdgeInsets.all(18),
                      maxLength: 20,
                      buildCounter: (BuildContext context, { int? currentLength, int? maxLength, bool? isFocused }) {

                        return Align(
                          alignment: Alignment.centerRight,
                          child: Text('${ currentLength }/${ maxLength }',
                            style: TextStyle(
                              color: (currentLength ?? 0) <= (maxLength ?? 0) ? Colors.grey : Colors.red,
                            ),
                          ),
                        );
                      },
                      text: 'Enter your title',
                    ),
                    const SizedBox(height: 18),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Are you the creator?",
                          style: TextStyle(color: Colors.grey, fontSize: 14)),
                        Switch(
                          value: _isAuthor,
                          onChanged: (value) {
                            if(!_isAuthor)
                              setState(() => _authorController.text = "");

                            setState(() => _isAuthor = value);
                          },
                          activeTrackColor: Colors.black,
                          activeColor: Colors.grey,
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    CustomField(
                      controller: _authorController,
                      autofocus: false,
                      readOnly: _isAuthor,
                      textColor: Colors.grey,
                      borderColor: Colors.grey[300],
                      backgroundColor: !_isAuthor ? Colors.white : Colors.grey[100],
                      maxLines: 1,
                      style: TextStyle(height: 1.5),
                      contentPadding: EdgeInsets.all(18),
                      maxLength: 20,
                      buildCounter: (BuildContext context, { int? currentLength, int? maxLength, bool? isFocused }) {

                        return Align(
                          alignment: Alignment.centerRight,
                          child: Text('${ currentLength }/${ maxLength }',
                            style: TextStyle(
                              color: (currentLength ?? 0) <= (maxLength ?? 0) ? Colors.grey : Colors.red,
                            ),
                          ),
                        );
                      },
                      text: 'Enter author name',
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _saveMetadata({ required BuildContext context }) {
    final String title = _titleController.text.trim();
    final String author = _authorController.text.trim();

    if(title.isNotEmpty) {

      if(!_isAuthor && author.isEmpty)
        return;

      mainStore.studio.addIsAuthor(isAuthor: this._isAuthor);
      mainStore.studio.addTitle(title: title);
      mainStore.studio.addAuthor(author: author);

      Navigator.pop(context);
    }
  }
}
