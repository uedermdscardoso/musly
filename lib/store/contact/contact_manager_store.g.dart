// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact_manager_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ContactManagerStore on _ContactManagerStore, Store {
  late final _$contactsAtom =
      Atom(name: '_ContactManagerStore.contacts', context: context);

  @override
  Future<List<Contact>>? get contacts {
    _$contactsAtom.reportRead();
    return super.contacts;
  }

  @override
  set contacts(Future<List<Contact>>? value) {
    _$contactsAtom.reportWrite(value, super.contacts, () {
      super.contacts = value;
    });
  }

  late final _$addContactsAsyncAction =
      AsyncAction('_ContactManagerStore.addContacts', context: context);

  @override
  Future addContacts({required bool withThumbnails}) {
    return _$addContactsAsyncAction
        .run(() => super.addContacts(withThumbnails: withThumbnails));
  }

  late final _$addNewContactAsyncAction =
      AsyncAction('_ContactManagerStore.addNewContact', context: context);

  @override
  Future addNewContact({required Contact contact}) {
    return _$addNewContactAsyncAction
        .run(() => super.addNewContact(contact: contact));
  }

  late final _$updateContactAsyncAction =
      AsyncAction('_ContactManagerStore.updateContact', context: context);

  @override
  Future updateContact({required Contact contact}) {
    return _$updateContactAsyncAction
        .run(() => super.updateContact(contact: contact));
  }

  late final _$deleteContactAsyncAction =
      AsyncAction('_ContactManagerStore.deleteContact', context: context);

  @override
  Future deleteContact({required Contact contact}) {
    return _$deleteContactAsyncAction
        .run(() => super.deleteContact(contact: contact));
  }

  late final _$_ContactManagerStoreActionController =
      ActionController(name: '_ContactManagerStore', context: context);

  @override
  dynamic clearContactList() {
    final _$actionInfo = _$_ContactManagerStoreActionController.startAction(
        name: '_ContactManagerStore.clearContactList');
    try {
      return super.clearContactList();
    } finally {
      _$_ContactManagerStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
contacts: ${contacts}
    ''';
  }
}
