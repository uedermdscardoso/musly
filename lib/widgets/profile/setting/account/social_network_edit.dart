
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/domain/model/utilities/social_network_type.dart';
import 'package:bridge/main.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/domain/model/user/users.dart';

class SocialNetworkEdit extends StatefulWidget {

  @override
  _SocialNetworkEditState createState() => _SocialNetworkEditState();

}

class _SocialNetworkEditState extends State<SocialNetworkEdit> {

  final TextEditingController _twitterController = TextEditingController();
  final TextEditingController _instagramController = TextEditingController();
  final TextEditingController _tiktokController = TextEditingController();

  final UserService _userService = UserService();
  Users _currentUser = Users();

  late Translate _intl;

  @override
  void initState() {
    mainStore.user.addCurrentUser();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Observer(
          builder: (_) {

            return StreamBuilder(
              stream: mainStore.user.currentUser,
              builder: (context, snapshot) {

                if(snapshot.hasData) {
                  final QuerySnapshot snap = snapshot.data as QuerySnapshot;
                  _currentUser = this._userService.makeUser(doc: snap.docs.first);

                  if(this._currentUser != null) {
                    this._twitterController.text = this._currentUser.twitter;
                    this._instagramController.text = this._currentUser.instagram;
                    this._tiktokController.text = this._currentUser.tiktok;
                  }
                }

                return Column(
                  children: [
                    Header(
                      color: Colors.transparent,
                      left: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () => Navigator.pop(context),
                            child: Container(
                              child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                            ),
                          ),
                          const SizedBox(width: 4),
                          Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_SOCIAL_NETWORK.TITLE') }',
                              style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
                        ],
                      ),
                      right: GestureDetector(
                        onTap: () async {
                          await _saveSocialNetwork();
                          Navigator.pop(context);
                        },
                        child: Container(
                            color: Colors.white,
                            child: Center(child: Text('${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_SOCIAL_NETWORK.BUTTON.SAVE') }', style: TextStyle(color: Colors.black, decoration: TextDecoration.none, fontSize: 16, fontWeight: FontWeight.normal, fontFamily: 'Arial')))
                        ),
                      ),
                      hasBorder: true,
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(16),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 12, left: 8, right: 8, bottom: 8),
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text('Edit Username', style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 12, fontWeight: FontWeight.normal, fontFamily: 'Arial'))
                              ),
                            ),
                            CustomField(
                              controller: _twitterController,
                              maxLines: 1,
                              autofocus: false,
                              obscureText: false,
                              prefixIcon: Image.asset('assets/icons/social_network/dark/bird_twitter.png', color: Colors.grey, scale: 2.25),
                              textAlign: TextAlign.start,
                              backgroundColor: Colors.white,
                              textColor: Colors.grey,
                              borderColor: Colors.grey[300],
                              text: '${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_SOCIAL_NETWORK.FIELD.ADD_TWITTER') }',
                            ),
                            const SizedBox(height: 18),
                            CustomField(
                              controller: _instagramController,
                              maxLines: 1,
                              autofocus: false,
                              obscureText: false,
                              prefixIcon: Image.asset('assets/icons/social_network/dark/instagram.png', color: Colors.grey, scale: 2.25),
                              textAlign: TextAlign.start,
                              backgroundColor: Colors.white,
                              textColor: Colors.grey,
                              borderColor: Colors.grey[300],
                              text: '${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_SOCIAL_NETWORK.FIELD.ADD_INSTAGRAM') }',
                            ),
                            const SizedBox(height: 18),
                            CustomField(
                              controller: _tiktokController,
                              maxLines: 1,
                              autofocus: false,
                              obscureText: false,
                              prefixIcon: Image.asset('assets/icons/social_network/dark/tiktok.png', color: Colors.grey, scale: 2.25),
                              textAlign: TextAlign.start,
                              backgroundColor: Colors.white,
                              textColor: Colors.grey,
                              borderColor: Colors.grey[300],
                              text: '${ _intl.translate('SCREEN.SETTING.MANAGER_ACCOUNT.EDIT_SOCIAL_NETWORK.FIELD.ADD_TIKTOK') }',
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                );
              },
            );
          }
        ),
      ),
    );
  }

  _saveSocialNetwork() async {
    final String twitterUsername = _twitterController.text;
    final String instagramUsername = _instagramController.text;
    final String tiktokUsername = _tiktokController.text;

    await mainStore.user.saveSocialNetwork(socialNetwork: twitterUsername, type: SocialNetworkType.TWITTER);
    await mainStore.user.saveSocialNetwork(socialNetwork: instagramUsername, type: SocialNetworkType.INSTAGRAM);
    await mainStore.user.saveSocialNetwork(socialNetwork: tiktokUsername, type: SocialNetworkType.TIKTOK);
  }
}
