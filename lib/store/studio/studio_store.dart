
import 'dart:async';
import 'dart:io';

import 'package:mobx/mobx.dart';
import 'package:bridge/domain/model/media/cutted_media_dto.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/domain/model/media/effect.dart';

part 'studio_store.g.dart';

class StudioStore = _StudioStore with _$StudioStore;

abstract class _StudioStore with Store {

}